Структура:
- Головна сторінка '/'
- Автори `users`
- Окремий автор `users/2`
- Блог `/posts`
- 404

Аватарки юзера https://pravatar.cc/
Дизайн https://www.figma.com/file/qtuQvYg4tTcN6f4Ip4Uszs/Router?type=design&node-id=0%3A1&mode=design&t=kqtkNxjnXRhwGBAD-1

1. Додайте в `Header` меню навігації.
2. Напишіть роути та додайте їх у програму
4. При переході на `/users` завантажувати та малювати авторів за посиланням `https://ajax.test-danit.com/api/json/users`;
6. При натисканні на окремого автора відкривати сторінку окремого автора, завантажувати та малювати інформацію за посиланням `https://ajax.test-danit.com/api/json/users/{userId}`;
3. При переході на `/posts` завантажувати та малювати пости за посиланням `https://ajax.test-danit.com/api/json/posts`;[
7. Додати `BackButton`;
9. Додати Go To Home button;
10. Додати 404 сторінку;
11. Реалізувати сторінку логіна та вхід на сторінку блогу лише для залогінених користувачів;
    ```html
    <form>
      <input
        type="text"
        name="name"
        placeholder="Name"
      />
      <input
        type="text"
        name="password"
        placeholder="Password"
      />
      <Button type="submit">Log In</Button>
    </form>
      ````
12. Відновлення скролла;