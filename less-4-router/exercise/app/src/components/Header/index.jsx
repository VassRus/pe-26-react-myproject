import styles from './Header.module.scss';
import { NavLink } from 'react-router-dom';

const Header = () => {

  return (
    <header className={styles.header}>

      <nav>
        <ul>
          <li>
            <NavLink to="/" className={({ isActive }) => isActive && styles.active}>Home</NavLink>
            <NavLink to="/users" className={({ isActive }) => isActive && styles.active}>Users</NavLink>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;
