import PropTypes from 'prop-types';
import styles from './UserCard.module.scss';
import { Link } from 'react-router-dom';



export default function UserCard({ id, name = '', username = '' }) {
    return (
        <Link to={`${id}`}>
            <div className={styles.card}>
                <img src={`https://i.pravatar.cc/300?v=${id}`} alt="img" />
                <p>{name}</p>
                <span>{username}</span>
            </div>
        </Link>
    )
}
/*export default function UserCard(users) {
    return (
        <Link to={`${users.id}`}>
            <div className={styles.card}>
                <img src={`https://i.pravatar.cc/300?v=${users.id}`} alt="img" />
                <p>{users.name}</p>
                <span>Username:{users.username}</span>
            </div>
        </Link>
    )
}*/
UserCard.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    username: PropTypes.string,
}
