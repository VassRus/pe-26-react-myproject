import { useState, useEffect } from 'react'
import './App.css'
import Header from "./components/Header"
import AppRouter from "./AppRouter"
function App() {
  const [users, setUsers] = useState([])
  useEffect(() => {
    try {
      const fetchUsers = async () => {
        const data = await fetch('https://ajax.test-danit.com/api/json/users').then(res => res.json());

        setUsers(data);
      }
      fetchUsers();
    } catch (err) {
      console.log(err)
    }
  }, [])
  console.log(users)

  return (
    <>
      <Header />

      <AppRouter users={users} />
    </>
  )
}
export default App
