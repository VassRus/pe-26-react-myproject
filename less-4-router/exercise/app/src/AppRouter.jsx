import HomeRoute from "./routes/HomeRoute"
import UsersRoute from "./routes/UsersRoute"
import { Routes, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import UserRoute from "./routes/UserRoute";

const AppRouter = ({ users = [] }) => {

  return (
    <Routes>

      <Route path="/" element={<HomeRoute />} />
      <Route path="/users" element={<UsersRoute users={users} />}>
      <Route path="/users/:id" element={<UserRoute />} />
      </ Route>

       {/* <Route path="/users/:id" element={<UserRoute />} />*/} {/*якщо за </Route> буде перекидати на орему сторінку*/}
    </Routes>
  )
}

AppRouter.propTypes = {
  users: PropTypes.array,
}

export default AppRouter;