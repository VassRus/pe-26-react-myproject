import {useParams} from "react-router-dom"
import { useState, useEffect } from "react";


export default function UserRoute() {
  const { id } = useParams()
  const [user, setUser] = useState({});
  const {name, username, email, phone, website } = user;
  useEffect(function() {
    async function getUser() {
      try{
        const response = await fetch(`https://ajax.test-danit.com/api/json/users/${id}`)
        const data = await response.json();
        console.log(data)
        setUser(data)
      }catch(err) {
        console.log(err)
      }
    }
    getUser()
  }, [id]);
  return (
    <div>
      <ul>
        <img src={`https://i.pravatar.cc/300?v=${id}`} alt="img" />
        <li> {name}</li>
        <li>Username: {username}</li>
        <li>Email: {email}</li>
        <li>Phone: {phone}</li>
        <li>Website: {website}</li>
      </ul>
    </div>
  )
}
