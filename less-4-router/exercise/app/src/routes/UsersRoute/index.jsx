
import PropTypes from 'prop-types';
import UserCard from '../../components/UserCard';
import { Outlet } from 'react-router-dom'

export default function UsersRoute({ users = [] }) {
  console.log(users)
  return (
    <div>
      <h1>USERS</h1>

      <div style={{display: 'flex', gap: 15, flexWrap: 'wrap'}}>

          {users.map((user) => (

         /* <UserCard key={id} name={name} username={username} id={id} />*/
              <UserCard key={user.id} {...user} />
        ))}
          <Outlet />
      </div>



    </div>
  )
}

UsersRoute.propTypes = {
  users: PropTypes.array,
}
