# [ReactRouter](https://reactrouter.com/en/main/start/overview)

## Configuration

[Picking a router](https://reactrouter.com/en/main/routers/picking-a-router)

<hr />

### [createBrowserRouter](https://reactrouter.com/en/main/routers/create-browser-router)
### [BrowserRouter](https://reactrouter.com/en/main/router-components/browser-router)

<hr />

### [Link](https://reactrouter.com/en/main/components/link)
### [NavLink](https://reactrouter.com/en/main/components/nav-link)

<hr />

## Hooks

* https://reactrouter.com/en/main/hooks/use-navigate
* https://reactrouter.com/en/main/hooks/use-location
* https://reactrouter.com/en/main/hooks/use-match
* https://reactrouter.com/en/main/hooks/use-params
