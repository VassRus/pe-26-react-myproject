import styles from './Header.module.scss';
import { NavLink } from 'react-router-dom';

const Header = () => {

  return (
    <header className={styles.header}>
      <span className={styles.logo}>React Router</span>

      <nav>
        <ul>
          <li>
            <NavLink to="/" className={({ isActive }) => isActive && styles.active}>Home</NavLink>
            <NavLink to="/blog" className={({ isActive }) => isActive && styles.active}>Blog</NavLink>
            <NavLink to="/contacts" className={({ isActive }) => isActive && styles.active}>Contacts</NavLink>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;
