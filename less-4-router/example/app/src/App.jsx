import { useEffect, useState } from 'react';
import './App.css';
import AppRouter from './AppRouter';
import Header from './components/Header';
function App() {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    try {
      const fetchPosts = async () => {
        const data = await fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json());
  
        setPosts(data);
      }

      fetchPosts();

    } catch(err) {
      console.log(err)
    }
  }, [])

  return (
    <>
    <Header />

    <AppRouter posts={posts} />
    </>
  )
}

export default App
