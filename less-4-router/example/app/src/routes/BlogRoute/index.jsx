import PropTypes from 'prop-types';
import { Link, Outlet } from 'react-router-dom';

export default function Blog({ posts = [] }) {
  console.log(posts)
  return (
    <div>
      <h1>Blogs</h1>

      <Outlet />
      
      <div>

        {posts.map(({ title, id }) =>
        <Link key={title} to={`/blog/${id}`}
              style={{ display: 'block', marginBottom: 10 }}
        >
            {title}
        </Link>)}
      </div>

    </div>
  )
}

Blog.propTypes = {
  posts: PropTypes.array,
}
