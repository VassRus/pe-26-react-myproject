import { useState, useEffect } from 'react';
import {Link, useParams} from 'react-router-dom';

export default function PostRoute() {
    const { id } = useParams();
    const [post, setPost] = useState([]);
    useEffect(() => {
        try {
          const fetchPost = async () => {
            const data = await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`).then(res => res.json());

            setPost(data);
             // console.log(data)
          }
      
          fetchPost();
        } catch(err) {
          console.log(err)
        }
      }, [])
  return (
    <div>
        {post && (
            <>
            <p>{ post.id }</p>
            <Link to="/blog/active" >{ post.title }</Link>
            <p>{ post.body }</p>
            </>
        )}
    </div>
  )
}
