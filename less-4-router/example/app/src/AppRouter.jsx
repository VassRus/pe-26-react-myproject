import {Routes, Route} from 'react-router-dom';
import HomeRoute from './routes/HomeRoute';
import BlogRoute from './routes/BlogRoute';
import PostRoute from './routes/PostRoute';
import PropTypes from 'prop-types';

const AppRouter = ({ posts = [] }) => {

    return (
      <Routes>
        <Route path="/" element={<HomeRoute   />} />
        <Route path="/blog" element={<BlogRoute posts={posts} />} >
          <Route path="/blog/active" element={<h1>ACTIVE</h1> }/>  {/*//  <Outlet /> див компон blog це вкладений роутер*/}
          <Route path="archive" element={<h1>ARCHIVE</h1> }/>  {/*//  <Outlet /> див компон blog це вкладений роутер*/}
        </Route>
          <Route path="/blog/:id" element={<PostRoute />} /> {/*//динамічний роутур -  див компонент  Blog - <Link key={title} to={`/blog/${id}`} + PostRoute */}

        <Route path="/contacts" element={<h1>Contacts</h1>} />

        <Route path="*" element={<h1>SRY! Can't found this page</h1>} />
      </Routes>
    )
}

AppRouter.propTypes = {
  posts: PropTypes.array,
}

export default AppRouter;