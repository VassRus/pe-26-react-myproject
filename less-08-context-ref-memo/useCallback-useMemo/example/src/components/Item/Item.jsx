import React, { memo } from 'react';
import PropTypes from 'prop-types';

const Item = (props) => {
    const { callback, another, array } = props;

    if (another) {
        console.log('ANOTHER IS HERE!')
    }

    if (array) {
        console.log('ARRAY IS HERE!')
    }

    return (
        <div className="container" style={{ background: 'lightgray' }}>
            <h3>{another ? 'ANOTHER' : ''} ITEM</h3>
            {array && array.map(el => <p key={el}>{el}</p>)}
            <button onClick={callback}>Callback</button>
        </div>
    )
}

Item.propTypes = {
    callback: PropTypes.func,
    another: PropTypes.bool,
    array: PropTypes.array,
};

Item.defaultProps = {
    callback: () => {},
    another: null,
    array: null,
};

export default memo(Item);