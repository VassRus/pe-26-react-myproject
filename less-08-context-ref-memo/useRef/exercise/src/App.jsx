import React, {useState, useRef} from 'react';
import classNames from 'classnames';
import styles from './App.module.scss';
import Input from './components/Input';

function App() {
  const [isOpen, setIsOpen] = useState(false);

  const inputRef = useRef(); // { current }
  const timeoutRef = useRef();

  function handleInput() {
    setIsOpen(prev => !prev);

    if(timeoutRef.current) {
      clearTimeout(timeoutRef.current);
      timeoutRef.current = null;
    }

    if(!isOpen) {
      timeoutRef.current = setTimeout(() => {
        inputRef.current.focus();
      }, 301)
    }
  }

console.log(inputRef);

  return (
    <div className={styles.App}>
      <div className={styles.header}>
        <button onClick={handleInput}>Search</button>
      </div>

      <div className={classNames(styles.searchContainer, { [styles.open]: isOpen })}>
        <Input ref={inputRef} />
        <button>OK</button>
      </div>
    </div>
  );
}

export default App;
