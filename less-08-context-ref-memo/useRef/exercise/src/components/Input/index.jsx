/* eslint-disable react/display-name */

import { useRef } from "react";
import { useImperativeHandle } from "react";
import { forwardRef } from "react";

const Input = forwardRef((props, ref) => {

    const localRef = useRef(null);

    useImperativeHandle(ref, () => ({
        focus: () => {
            localRef.current.focus();
            setTimeout(() => {
                alert('!!!!!!!!!!!!!!')
            }, 1000)
        },
    }))

    return <input ref={localRef} type="search" name="search"/>
})

export default Input;
