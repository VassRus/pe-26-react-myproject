import PropTypes from 'prop-types';
import styles from './ShowMore.module.scss';
import classNames from 'classnames';
import { useRef, useEffect, useState } from 'react';


const ShowMore = ({ text }) => {
    const paragraphRef = useRef(null);
    const [height, setHeight] = useState(0);
    const [isExpanded, setIsExpanded] = useState(true);

    useEffect(() => {
        setHeight(paragraphRef.current.getBoundingClientRect().height)
    }, [])

    const isShowMore = height > 100;


    return (
        <div className={styles.root}>
            <div className={styles.textWrapper} style={{ height: isExpanded ? 'auto' : 100 }} >
                <p ref={paragraphRef} className={classNames(styles.textContainer, 'show-more')}>{text}</p>
            </div>
            {isShowMore && <button onClick={() => { setIsExpanded(prev => !prev) }} type='button'>Show {isExpanded ? 'less' : 'more'}</button>}
        </div>
    )
}

ShowMore.propTypes = {
    text: PropTypes.string.isRequired,
};

export default ShowMore;