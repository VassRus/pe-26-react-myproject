import  {useState} from 'react';
import './App.scss';
 import ShowMore from "./components/ShowMore";
import DataSaver from "./components/DataSaver";
 const text = 'DevTools failed to load source map: Could not load content for chrome-extension://ojnikmlgjpfiogeijjkpeakbedjhjcch/toggle_icon.js.map: HTTP error: status code 404, net::ERR_UNKNOWN_URL_SCHEME';

function App() {
  const [value, setValue] = useState(false);
  // console.log('HELLO FROM APP');

  return (
    <div className="App">
      <h1>{value ? 'true' : 'false'}</h1>
      <button onClick={() => setValue(prev => !prev)}>Change state</button>
      <DataSaver value={value} />

       <ShowMore text={text} />
      <ShowMore text='Hello from APp.jsx ' />
    </div>
  );
}

export default App;
