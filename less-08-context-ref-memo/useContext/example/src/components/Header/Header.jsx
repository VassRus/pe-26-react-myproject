import PropTypes from "prop-types";
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";

import DarkSVG from "../../assets/DarkSVG/index.jsx";
import LightSVG from "../../assets/LightSVG/index.jsx";
import { useContext } from "react";
import ThemeContext from "../../context/ThemeContext/index.jsx";
import classNames from 'classnames';
import lightTheme from '../../styles/light.module.scss';
import darkTheme from '../../styles/dark.module.scss';
import AContext from "../../context/AContext/index.jsx";

const Header = (props) => {
    const { title } = props;

    const { isLight, setIsLight } = useContext(ThemeContext);
    const { a, setA } = useContext(AContext);

    console.log(lightTheme)

    return (
        <header className={classNames(styles.root, {[lightTheme.header]: isLight, [darkTheme.header]: !isLight})}>
            <span>{a ? title : 'skjdfhksjhdkfjhskdjfhksdjfhksjdfh'}</span>
            <button onClick={() => { setA(prev => !prev) }}>Change</button>
            <nav>
                <ul>
                    <li>
                        <NavLink to='/' exact activeClassName={styles.activeLink}>Home</NavLink>
                    </li>
                    <li>
                        <NavLink to='/users' activeClassName={styles.activeLink}>Users</NavLink>
                    </li>
                </ul>
            </nav>

            <button
                className={styles.themeBtn}
                onClick={() => { setIsLight(prev => !prev) }}
            >{!isLight ? <LightSVG /> : <DarkSVG />}</button>

        </header>
    );
};

Header.propTypes = {
    title: PropTypes.string,
};

Header.defaultProps = {
    title: "Hello",
};

export default Header;
