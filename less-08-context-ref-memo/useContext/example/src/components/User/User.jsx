import  { useContext } from 'react';
import PropTypes from 'prop-types';
import styles from './User.module.scss';
import ThemeContext from '../../context/ThemeContext';
import classNames from 'classnames';
import lightTheme from '../../styles/light.module.scss';
import darkTheme from '../../styles/dark.module.scss';

const User = (props) => {
    const { name, avatar } = props;
    const { isLight } = useContext(ThemeContext);

    if (!name) return null;

    return (
        <div className={classNames(styles.user, {
            [lightTheme.user]: isLight,
            [darkTheme.user]: !isLight,
         })}>
            <h3>{name}</h3>
            <img src={avatar} alt={name} width={250} height={250}/>
        </div>
    )
}

User.propTypes = {
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string
};
User.defaultProps = {
    avatar: null,
};

export default User;
