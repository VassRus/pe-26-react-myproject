import './App.scss';
import Header from "./components/Header";
import Routes from "./Routes";
import { BrowserRouter } from "react-router-dom";
import ThemeContext from "./context/ThemeContext/index.jsx";
import classNames from 'classnames';
import lightTheme from './styles/light.module.scss';
import darkTheme from './styles/dark.module.scss';
import { useContext } from 'react';
import AProvider from './context/AContext/AProvider.jsx';

function App() {
  const { isLight } = useContext(ThemeContext);

  return (
    <BrowserRouter>

      <div className={classNames("App", {
        [lightTheme.App]: isLight,
        [darkTheme.App]: !isLight,
      })}>
        <AProvider>
          <Header title="React Context" />
        </AProvider>
        <Routes />
      </div>
    </BrowserRouter>
  );
}

export default App;
