import React from "react";
import User from "../../components/User";

const users = [
    { id: 1, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 2, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 3, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 4, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 5, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 6, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 7, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 8, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 9, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 10, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 11, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 12, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 13, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 14, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 15, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 16, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
    { id: 17, name: 'Anonymous', avatar: `https://i.pravatar.cc/250?v=${Math.random()}`},
]

const UsersPage = () => (
    <section>
        <h1>USERS</h1>

        <div style={{ display: 'flex', flexWrap: 'wrap'}}>
            {users.map(({ id, name, avatar }) => <User key={id} id={id} name={name} avatar={avatar} />)}
        </div>
    </section>
);


export default UsersPage;

