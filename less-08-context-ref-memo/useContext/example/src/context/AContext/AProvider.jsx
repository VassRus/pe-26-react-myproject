import AContext from ".";
import { useState } from "react";
import PropTypes from 'prop-types';

const AProvider = ({ children }) => {
    const [a, setA] = useState(true);

    const value = {
        a,
        setA,
    }

    return (
        <AContext.Provider value={value} >
            {children}
        </AContext.Provider>
    )
}

AProvider.propTypes = {
    children: PropTypes.oneOfType([ PropTypes.node, PropTypes.arrayOf([PropTypes.node]) ]),
}

export default AProvider;