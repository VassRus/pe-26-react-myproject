import { createContext } from "react";

const AContext = createContext(null);

export default AContext;