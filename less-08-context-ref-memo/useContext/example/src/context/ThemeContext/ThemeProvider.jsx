import ThemeContext from ".";
import { useEffect, useState } from "react";
import PropTypes from 'prop-types';

// це наш провайдер як оремий компонент
const ThemeProvider = ({ children }) => {
    const [isLight, setIsLight] = useState(true);

    const value = {
        isLight,
        setIsLight,
    }

  /*  useEffect(() => {
        if (localStorage.getItem('isLight')) {
            setIsLight(localStorage.getItem('isLight') === 'true')
        }
    }, [])

    useEffect(() => {
        localStorage.setItem('isLight', isLight ? 'true' : 'false')
    }, [isLight])*/

    return (
        <ThemeContext.Provider value={value} >
            {children}
        </ThemeContext.Provider>
    )
}

ThemeProvider.propTypes = {
    children: PropTypes.oneOfType([ PropTypes.node, PropTypes.arrayOf([PropTypes.node]) ]),
}

export default ThemeProvider;
