
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";
import LangSelect from "../LangSelect";

const Header= (props) => {
        const { title } = props;

        return (
              <header className={styles.root}>
                  <span>{title}</span>
                  <nav>
                      <ul>
                          <li>
                              <NavLink to='/' exact activeClassName={styles.activeLink}>Home</NavLink>
                          </li>
                          <li>
                              <NavLink to='/post' activeClassName={styles.activeLink}>Post</NavLink>
                          </li>
                      </ul>
                  </nav>

                  <LangSelect />
              </header>
        );
};

Header.propTypes = {
    title: PropTypes.string ,
};

Header.defaultProps = {
    title: "Hello",
};

export default Header;
