import  { useContext} from 'react';
import LanguageContext  from '../../context';

const LangSelect= () => {
    const {currentLanguage, setCurrentLanguage} = useContext(LanguageContext)

        return (
              <div>
                  <select
                      name="lang"
                      id="lang"
                      value={currentLanguage}
                      onChange={({ target: { value } }) => setCurrentLanguage(value)}
                  >
                      <option value='EN'>EN</option>
                      <option value='DE'>DE</option>
                      <option value='FR'>FR</option>
                      <option value='UA'>UA</option>
                  </select>
              </div>
        );
};

export default LangSelect;
