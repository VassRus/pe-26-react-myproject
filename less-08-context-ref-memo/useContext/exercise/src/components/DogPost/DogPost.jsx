import  {useContext} from 'react';
import  LanguageContext from '../../context';
import PropTypes from "prop-types";

const DogPost = () => {
    const { currentLanguage } = useContext(LanguageContext);

    return (
              <>
                  <img width={480} height={360} style={{ backgroundColor: 'lightgray' }} src="https://upload.wikimedia.org/wikipedia/en/4/45/DJ_Dog.gif" alt="DJ DOG"/>
                  { currentLanguage === 'EN' && <p>It's an edited picture of the dog (Indian Spitz) who is rocking on DJ.</p> }
                  { currentLanguage === 'DE' && <p>Es ist ein bearbeitetes Bild des Hundes (Indian Spitz), der auf DJ rockt.</p> }
                  { currentLanguage === 'FR' && <p>C'est une photo retouchée du chien (Indian Spitz) qui se balance sur DJ.</p> }
                  { currentLanguage === 'UA' && <p>Це відредагований малюнок собаки (індійського шпіца), який качає на DJ.</p> }
              </>
        );
};

DogPost.propTypes = {
    currentLanguage: PropTypes.string,
};

DogPost.defaultProps = {
    currentLanguage: 'EN',
}

export default DogPost;
