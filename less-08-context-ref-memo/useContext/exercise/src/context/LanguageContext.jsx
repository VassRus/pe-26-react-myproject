import {  useState } from "react";
import LanguageContext  from ".";


function LanguageProvider({children}) {
  const [currentLanguage, setCurrentLanguage] = useState('EN');

  const value = {
      currentLanguage,
      setCurrentLanguage,
  }

  return (

    <LanguageContext.Provider value={value}>
      {children}
    </LanguageContext.Provider>
  )
}

export {LanguageProvider}

