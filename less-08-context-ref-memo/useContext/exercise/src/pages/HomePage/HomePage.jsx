import React from "react";
import { Link } from "react-router-dom";

const HomePage = () => (
    <section>
        <h1>HOME</h1>
        <p>Don't miss our amazing <Link to='/post'>DOG POST</Link></p>
    </section>
)

export default HomePage;