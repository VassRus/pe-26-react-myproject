
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import Header from "./components/Header";
import Routes from "./Routes";
import { LanguageProvider } from './context/LanguageContext';

function App() {
  return (
    <BrowserRouter>
    <LanguageProvider>
      <div className="App">
        <Header title="Context" />
        <Routes />
      </div>
      </LanguageProvider>
    </BrowserRouter>
  );
}

export default App;
