# Todo application


## Components

1. Header (**App Bar**)
    * Title
    * Icon (**List Alt**) with numbers of unchecked todo (**Badge**)

2. Title with icon (**AddReactionOutlined**)
    * by hovering on icon user should see the tooltip with the text "Fill the form and press the button"

3. Form for adding todo

4. List of todo items


## Todo item

1. Should include title, description and checkbox

2. Description should be hidden by default and open if user click on the title (**Accordion**)

3. By clicking checkbox tasks should be marked as done


## Additional logic

1. By adding new todo show notification to user that new todo added (**Snackbar**)

2. Show loading process somehow (**Backdrop**)


