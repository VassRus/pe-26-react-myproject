import { useState } from 'react'
import './App.css'
import Button from '@mui/material/Button';
import Test from "./components/Test/index.jsx";

function App() {
  const [count, setCount] = useState(0)



  return (
    <div>
        <Test/>

        <Button className='button-test' sx={{ padding: 5, fontSize: '16px' }} variant='outlined'>Hello</Button>

    </div>
  )
}

export default App
