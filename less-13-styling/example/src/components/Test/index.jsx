import styles from './Test.module.scss';
import Select from 'react-select'
import {css} from "@emotion/css";
import styled from '@emotion/styled'


const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
]

const Paragraph = styled.p`
    padding: 32px;
    background-color: #003cff;
    font-size: 24px;
    border-radius: 4px;
    color: black;
    font-weight: bold;

    &:hover {
        color: white;
    }
`;

export default function Test() {
    return (
        <div className={styles.root}>
            <h1>Hello</h1>

            <Select options={options}/>


            <button
                className={css`
                    padding: 32px;
                    background-color: #fff200;
                    font-size: 24px;
                    border-radius: 4px;
                `}
                > Button </button>
            <Paragraph>Lorem, ipsum dolor.</Paragraph>
            <Paragraph>Second paragraph</Paragraph>
        </div>


    )
}
