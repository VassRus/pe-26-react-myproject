import { useField } from "formik"
import TextField from '@mui/material/TextField';

export default function Input(props) {
    const [field] = useField(props)

  return (
    <div>
        <TextField
            maxRows={4} sx={{ width: '80%' }}
            {...field}
            {...props}
        />
    </div>
  )
}
