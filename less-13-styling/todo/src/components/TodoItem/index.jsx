import Grid from '@mui/material/Grid';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Checkbox from '@mui/material/Checkbox';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';

export default function TodoItem({ title = '', description = '', isDone = true }) {

    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

    return (
        <Box sx={{ padding: 1 }}>

            <Grid container spacing={2}>
                <Grid item xs={10}>
                    <Accordion>
                        <AccordionSummary aria-controls="panel1-content" id="panel1-header"
                        >
                            {title}
                        </AccordionSummary>

                        <AccordionDetails>
                            {description}
                        </AccordionDetails>
                    </Accordion>
                </Grid>

                <Grid item xs={2}>
                    <Checkbox {...label} checked={isDone} />
                </Grid>

            </Grid>
        </Box>
    )
}


TodoItem.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    isDone: PropTypes.bool
  };
