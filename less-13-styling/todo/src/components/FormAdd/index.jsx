import { Formik, Form } from 'formik';
import Input from '../Input';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import { useDispatch } from 'react-redux';
import { addTodo } from '../../redux/todoSlice';
import { useState } from 'react';
import Snackbar from '@mui/material/Snackbar';

const FormAdd = () => {

    const [snackbar, setSnackBar] = useState({ isOpen: false, title: '' })

    const handleClose = () => {
        setSnackBar({ isOpen: false, title: '' })
    }

    const initialValues = {
        title: '',
        description: '',
    }

    const dispatch = useDispatch();

    const onSubmit = (values, { resetForm }) => {
        dispatch(addTodo(values));
        resetForm();
        setSnackBar({ isOpen: true, title: `${values.title} was added! :)` })
    }


    return (
        <Box sx={{ padding: 2  }}>
            <Formik initialValues={initialValues} onSubmit={onSubmit}>
                <Form>
                    <Grid container spacing={2}>

                        <Grid item xs={8}>
                            <Input sx={{ margin: 2 , width: 700}}
                                name="title"
                                placeholder="My title"
                                label='Title'
                            />


                            <Input sx={{ margin: 2 , width: 700}}
                                name="description"
                                placeholder="Go to the shop with Marry"
                                label='Description'
                            />
                        </Grid>

                        <Grid item xs={2}>
                            <Button type="submit" variant='contained'>Add</Button>
                        </Grid>
                    </Grid>
                </Form>
            </Formik>

            <Snackbar
                open={snackbar.isOpen}
                autoHideDuration={2000}
                onClose={handleClose}
                message={snackbar.title}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            />
        </Box>
    )
}

export default FormAdd;
