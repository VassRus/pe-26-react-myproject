import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Badge from '@mui/material/Badge';
import { useSelector } from 'react-redux';

const Header = () => {

    const todos = useSelector(state => state.todos.data.length)

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Add Todo!
                    </Typography>

                    <Badge badgeContent={todos} color="warning">
                        <Typography variant="h6" component="p">todos</Typography>
                    </Badge>
                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default Header;