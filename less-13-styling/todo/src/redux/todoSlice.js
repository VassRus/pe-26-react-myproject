import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { addTodoApi, getAllTodoApi } from '../utils/api';

export const addTodo = createAsyncThunk(
    'todos/addTodo',
    async (todo) => {
        const { data } = await addTodoApi(todo);
        return data;
    }
);

export const fetchAllTodo = createAsyncThunk(
    'todos/fetchAllTodo',
    async () => {
        const { data } = await getAllTodoApi();
        return data;
    }
);

const initialState = {
    data: [],
    isLoading: false,
    isError: false,
};

export const todoSlice = createSlice({
    name: 'todos',
    initialState,

    extraReducers: (builder) => {

        // addTodo ---------------------------------------------------------------
        builder.addCase(addTodo.fulfilled, (state, action) => {
            state.data.push({ ...action.payload, isDone: false })
            state.isLoading = false;
        })

        builder.addCase(addTodo.pending, (state) => {
            state.isLoading = true;
        })

        builder.addCase(addTodo.rejected, (state) => {
            state.isLoading = false;
            state.isError = true;
        })

        // fetchAllTodo ---------------------------------------------------------------

        builder.addCase(fetchAllTodo.fulfilled, (state, action) => {
            state.data = action.payload;
            state.isLoading = false;
        })

        builder.addCase(fetchAllTodo.pending, (state) => {
            state.isLoading = true;
        })

        builder.addCase(fetchAllTodo.rejected, (state) => {
            state.isLoading = false;
            state.isError = true;
        })
    }
})

export default todoSlice.reducer