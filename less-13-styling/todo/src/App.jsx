import Header from "./components/Header"
import Typography from '@mui/material/Typography';
import smileIcon from './assets/emoticon-devil-outline.svg';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import styles from './App.module.scss';
import { styled } from '@mui/material/styles';
import FormAdd from "./components/FormAdd";
import TodoItem from "./components/TodoItem";
import { useDispatch, useSelector } from "react-redux";
import { fetchAllTodo } from "./redux/todoSlice";
import { useEffect } from "react";

const LightTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11,
  },
}));

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllTodo());
  }, [])

  const todos = useSelector(state => state.todos.data);


  return (
    <>
      <Header />

      <Typography className={styles.title} sx={{ m: 3 }} variant="h3"
      >
        Add you todo now!
      <div className={styles.iconWrapper}>
          <LightTooltip className={styles.tooltip} title="Fill the form and press the button" placement="top-start">
            <img src={smileIcon} alt="emoji" width={40} height={40} />
          </LightTooltip>
      </div>
      </Typography>

      <FormAdd />


        { todos.map(todo => <TodoItem key={todo.id} {...todo}/>) }

    </>
  )
}

export default App
