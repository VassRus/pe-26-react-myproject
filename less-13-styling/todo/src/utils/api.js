import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://ajax.test-danit.com/api/v2/cards',
    headers: {
        'Authorization': `Bearer 206e7623-60c6-4d42-b642-449d90a80dec`
    }
});

export const addTodoApi = async (todo) => {
    return await instance.post('', todo);
};

export const getAllTodoApi = async () => {
    return await instance.get('');
};


