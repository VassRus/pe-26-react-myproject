# Redux

- [Motivation](https://redux.js.org/understanding/thinking-in-redux/motivation)
- [3 principles](https://redux.js.org/understanding/thinking-in-redux/three-principles)
- [Glossary](https://redux.js.org/understanding/thinking-in-redux/glossary)

# Redux Toolkit

- [Quick Start](https://redux.js.org/tutorials/quick-start)
- [Selectors](https://redux.js.org/usage/deriving-data-selectors)
