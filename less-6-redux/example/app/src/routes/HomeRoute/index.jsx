import { useSelector, useDispatch } from "react-redux";
import { increment, decrement } from "../../redux/counterSlice";
import {openModal, closeModal, setModalText, setModalActions} from '../../redux/modalSlice';

const HomeRoute = () => {

   /* const {value} = useSelector(state => state.counter);*/
   /* або ще варіант нижче */
    const counter = useSelector(state => state.counter.value);

    const dispatch = useDispatch();


  return (
    <section>
      {/*<h1>{ value }</h1>*/}
      <h1>{ counter }</h1>

      <div className="container">

        <button onClick={() => {
            dispatch(setModalText({ title: 'INCREMENT', description: 'Do you really want to INCREMENT counter?????' }))
            dispatch(setModalActions('increment'))
            dispatch(openModal());

          /*  dispatch(increment())*/


        }}>+</button>

       {/*   <button onClick={() => { dispatch(increment()) }}>+</button>*/}
         {/* <button onClick={() => { dispatch(decrement()) }}>-</button>*/}
          <button onClick={() => {

              dispatch(setModalText({ title: 'DECREMENT', description: 'Do you really want to DECREMENT counter?????' }))
              dispatch(setModalActions('decrement'))
              dispatch(openModal())
             /* dispatch(decrement())*/

          }}>-</button>

      </div>
    </section>
  )
}

export default HomeRoute;
