import { useDispatch } from "react-redux";
import {increment, reset} from "../../redux/counterSlice";
import { openModal, setModalActions, setModalText } from '../../redux/modalSlice';


const ResetRoute = () => {
  const dispatch = useDispatch();

  return (
    <section>
      {/*<h1>Reset counter</h1>*/}

      <div className="container">

       {/* <button onClick={() => { { dispatch(reset())} }}>Reset counter </button>*/}


         <button onClick={() => {
         dispatch(setModalText({ title: 'RESET', description: 'Do you really want to reset counter?????' }));
         dispatch(openModal());
         dispatch(setModalActions('reset'));

            }}>Reset counter </button>

      </div>
    </section>
  )
}

export default ResetRoute;
