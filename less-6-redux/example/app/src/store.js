import { configureStore } from '@reduxjs/toolkit';
import counterReducer from './redux/counterSlice';
import modalReducer from './redux/modalSlice';

const store = configureStore({
    reducer: {
        counter: counterReducer,
        modal: modalReducer,
    },
});

export default store;