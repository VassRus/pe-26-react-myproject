import Header from "./components/Header/index.jsx";
import AppRouter from "./AppRouter.jsx";
import Modal from "./components/Modal/index.jsx";

function App() {

  return (
    <>
      <Header />
      <Modal />

      <main>
        <AppRouter />
      </main>
    </>
  )
}

export default App
