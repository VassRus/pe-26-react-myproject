import { createSlice } from '@reduxjs/toolkit';

const modalSlice = createSlice({
    name: 'modal',
    initialState: {
        isOpen: false,
        title: '',
        description: '',
        actions: null,
    },
    reducers: {
        openModal: state => {
            state.isOpen = true;
        },

        closeModal: state => {
            state.isOpen = false;
        },

        setModalText: (state, action) => {
            state.title = action.payload.title;
            state.description = action.payload.description;
        },

        setModalActions: (state, action) => {
            state.actions = action.payload;
        }
    }
});

export const { openModal, closeModal,
    setModalText, setModalActions } = modalSlice.actions;

export default modalSlice.reducer;


