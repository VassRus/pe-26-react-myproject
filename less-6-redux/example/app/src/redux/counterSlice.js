import { createSlice } from '@reduxjs/toolkit';


const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        value: 0,
        actionsCount: 0,
    },
    reducers: {
        increment: (state, action) => {
            state.value++;
            state.actionsCount++;
        },

        decrement: state => {
            state.value--;
            state.actionsCount++;
        },

        reset: state => {
            state.value = 0;
            state.actionsCount =0;
        }
    }
});

export const { increment, decrement,
    reset } = counterSlice.actions;

export default counterSlice.reducer;


