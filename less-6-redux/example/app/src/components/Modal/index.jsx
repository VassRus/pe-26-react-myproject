import { useDispatch, useSelector } from 'react-redux';
import styles from './Modal.module.scss';
import { closeModal } from '../../redux/modalSlice';
import {decrement, increment, reset} from '../../redux/counterSlice';
const Modal = () => {
    const { isOpen, title, description, actions } = useSelector(state => state.modal);
    const dispatch = useDispatch();
    if (!isOpen) {
        return null;
    }
    return (
        <div className={styles.root}>
            <div className={styles.background} onClick={()=>dispatch(closeModal()) }  />
            <div className={styles.contentContainer}>
                <h1>{ title }</h1>
                <p>{ description }</p>
                <button onClick={() => {
                    if (actions === 'decrement') {
                        dispatch(decrement())
                    }else if (actions === 'increment') {
                        dispatch(increment())
                    }else if (actions === 'closeModal') {
                        dispatch(closeModal())
                    }else if (actions === 'reset') {
                        dispatch(reset())
                    }
                }}> Yes </button>
                <br/>
                <button onClick={()=>dispatch(closeModal()) } >No</button>

                {/*<button onClick={() => { dispatch(closeModal()) }}>No</button>*/}
            </div>
        </div>
    )
}
export default Modal;