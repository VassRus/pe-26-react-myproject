import { useSelector } from 'react-redux';
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";

const Header = () => {
  const { value, actionsCount } = useSelector(state => state.counter);


  return (
    <header className={styles.header}>
      <span className={styles.logo}>React Router</span>

      <h4>Counter: { value }</h4>
      <h4>Actions: { actionsCount }</h4>

      <nav>
        <ul>
          <li>
            <NavLink end to="/" className={({ isActive }) => isActive && styles.active}>Counter</NavLink>
          </li>
          <li>
            <NavLink end to="/reset" className={({ isActive }) => isActive && styles.active}>Reset</NavLink>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;
