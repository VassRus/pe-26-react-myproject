import { Routes, Route } from 'react-router-dom';
import HomeRoute from './routes/HomeRoute/index.jsx';
import ResetRoute from "./routes/ResetRoute/index.jsx";


const AppRouter = () => {

  return (
    <Routes>
     <Route path='/' element={<HomeRoute />} />
     <Route path='/reset' element={<ResetRoute />} />
    </Routes>
  )
}


export default AppRouter;
