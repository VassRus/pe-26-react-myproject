import { createSlice } from '@reduxjs/toolkit';


const modalSlice = createSlice({
    name: 'modal',
    initialState: {
        isOpen: false,
        text: '',
        id: null,
    },
    reducers: {
        openModal: state => {
            state.isOpen = true;
        },

        closeModal: state => {
            state.isOpen = false;
        },

        configureModal: (state, action) => {
            state.text = action.payload.text;
            state.id = action.payload.id;
        },

    }
});

export const { openModal, closeModal, configureModal } = modalSlice.actions;

export default modalSlice.reducer;


