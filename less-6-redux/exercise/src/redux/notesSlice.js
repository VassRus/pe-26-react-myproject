import { createSlice } from "@reduxjs/toolkit";

const initialState = {
 data: [

]
}

const notesSlice = createSlice({
  name: 'notes',
  initialState,
  reducers: {

    setInitialNote(state, action) {
      state.data = action.payload;
    },

    addNote(state, action) {
      state.data.push({
        id: state.data.length,
        text: action.payload,
        isDone: false,
      });
      
    localStorage.setItem('notes', JSON.stringify(state.data))
    },

    deleteNode(state, action) {
      const index = state.data.findIndex(todo => todo.id === action.payload)
      if (index !== -1) state.data.splice(index, 1)
      localStorage.setItem('notes', JSON.stringify(state.data))
    },
    
    toggleIsDone(state, action) {
      const note = state.data.find(note => note.id === action.payload);
      note.isDone = !note.isDone;
      localStorage.setItem('notes', JSON.stringify(state.data))
    }
  }
})

export const {addNote, deleteNode,
  toggleIsDone, setInitialNote} = notesSlice.actions;

export default notesSlice.reducer;