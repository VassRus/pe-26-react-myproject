import {configureStore} from "@reduxjs/toolkit";

import notesSlice from "./redux/notesSlice";
import modalSlice from "./redux/modalSlice";

const store = configureStore({
  reducer : {
    notes: notesSlice,
    modal: modalSlice,
  }
});

export default store;