import { useEffect } from 'react';
import './App.css';
import AddNote from "./components/AddNote/index.jsx";
import NoteContainer from "./components/NoteContainer/index.jsx";
import Modal from './components/Modal/index.jsx';

import {useDispatch} from 'react-redux';

import { setInitialNote } from './redux/notesSlice.js';

function App() {

  const dispatch = useDispatch();

  useEffect(() => {
    const notes = localStorage.getItem('notes');

    if (notes) {
      dispatch(setInitialNote(JSON.parse(notes)))
    }
  }, [])

  return (
    <div className="App">
      <AddNote />
      <NoteContainer />
      <Modal />
    </div>
  );
}

export default App;
