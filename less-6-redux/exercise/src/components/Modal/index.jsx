import { useDispatch, useSelector } from 'react-redux';
import styles from './Modal.module.scss';
import { closeModal } from '../../redux/modalSlice';
import { deleteNode } from '../../redux/notesSlice';


const Modal = () => {
    const { isOpen, text, id } = useSelector(state => state.modal);
    const dispatch = useDispatch();

    function handleDeleteModal() {
        dispatch(deleteNode(id))
        dispatch(closeModal())
    }

    if (!isOpen) {
        return null;
    }

    return (
        <div className={styles.root}>

            <div className={styles.background} onClick={() => { dispatch(closeModal()) }} />
            
            <div className={styles.contentContainer}>
                <h1>Do you want to delete { text } ?</h1>

                <button onClick={handleDeleteModal}>Yes</button>
                <button onClick={() => { dispatch(closeModal()) }}>No</button>
            </div>
        </div>
    )
}

export default Modal;