import classNames from "classnames";
import PropTypes from 'prop-types';
import styles from './NoteItem.module.scss';
import EditSVG from "../svg/EsidSVG.jsx";
import DeleteSVG from "../svg/DeleteSVG.jsx";

import {useDispatch} from 'react-redux';

import { toggleIsDone } from "../../redux/notesSlice.js";

import { openModal, configureModal } from "../../redux/modalSlice.js";

const NoteItem = (props) => {
  const { index, text, id, isDone } = props;

  const dispatch = useDispatch();

  function handleDeleteNote() {
    dispatch(openModal())
    dispatch(configureModal({text, id}))
  }

  function handleIsDone() {
    dispatch(toggleIsDone(id))
  }

  return (
    <li className={classNames(styles.root, { [styles.rootDone]: isDone })}>
      <div className={styles.wrapper}>

        <input checked={isDone} readOnly={true} type="checkbox" className={styles.checkbox} onClick={handleIsDone}/>

        <span>{index}.</span>
        <p className={classNames({ [styles.done]: isDone })}>{text}</p>
      </div>

      <div className={styles.wrapper}>
        <button className={classNames(styles.btn, styles.primary)}><EditSVG /></button>
        <button className={classNames(styles.btn, styles.error)} onClick={handleDeleteNote}><DeleteSVG /></button>
      </div>
    </li>
  )
}

NoteItem.propTypes = {
  index: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  text: PropTypes.string,
};
NoteItem.defaultProps = {
  text: '',
};

export default NoteItem;
