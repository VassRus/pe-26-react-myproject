import { useState } from 'react';
import styles from './AddNote.module.scss';
import {useDispatch} from 'react-redux'
import { addNote } from '../../redux/notesSlice';

const AddNote = () => {
  const [value, setValue] = useState(''); // цей стейт нашого input (для того щоб зберігались наші дані які ми вводимо в input)
  const dispatch = useDispatch();

  function handleAddNote() { // функція яка добавляє дані
    dispatch(addNote(value)) // добавляємо дані через input

      setValue(''); // очистити input
  }

  return (
    <>
      <div className={styles.root}>
        <input className={styles.input}
          type='text' placeholder='Your note'

          value={value} // закріплюємо наші дані
          onChange={(e) => {setValue(e.target.value)}} // це наші перезаписані дані те що вводимо в input
        />
        <button className={styles.btn} onClick={handleAddNote}>Add Note</button>
      </div>
      <div className={styles.line} />
    </>
  )
}

export default AddNote;
