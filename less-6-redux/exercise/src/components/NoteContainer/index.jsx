import PropTypes from 'prop-types';
import NoteItem from "../NoteItem/index.jsx";
import {useSelector} from 'react-redux';

const NoteContainer = () => {
  const notes = useSelector( store => store.notes.data);

  if (!notes)
    return <p style={{ fontSize: 30 }}>You don&apos;t have any notes yet</p>;

  return (
    <ul>
      {notes && notes.map(({ text, id, isDone }, index) => <NoteItem index={index + 1} text={text} key={text} id={id} isDone={isDone}/>)}
    </ul>
  )
}

NoteContainer.propTypes = {
  notes: PropTypes.array,
};
NoteContainer.defaultProps = {
  notes: null,
};

export default NoteContainer;
