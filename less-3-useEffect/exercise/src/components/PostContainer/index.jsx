import PropTypes from 'prop-types'
import Post from '../Post'

const PostsContainer = ({posts = [], isLoading = false, handleFavourite = () => {}}) => {

  return <div>
    {isLoading && <div>Is Loading...</div>}

    {posts && posts.map(({title, body, id, isFavourite}) =>
        <Post
            key={id}
            title={title}
            body={body} id={id}
            isFavourite={isFavourite}
            handleFavourite={handleFavourite}
        />)}
  </div>

{/*    {posts && posts.map((post) =>
        <Post key={post.id} title={post.title} body={post.body} id={post.id} isFavourite={post.isFavourite} handleFavourite={handleFavourite}/>)}
  </div>*/}

   {/* {posts && posts.map((post) =>
        <Post {...post}  handleFavourite={handleFavourite}/>
    )}
  </div>*/}
}

PostsContainer.propTypes = {
  posts: PropTypes.array,
  isLoading: PropTypes.bool,
  handleFavourite: PropTypes.func,
}

export default PostsContainer