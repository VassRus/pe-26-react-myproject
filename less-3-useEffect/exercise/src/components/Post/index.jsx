import PropTypes from 'prop-types'
import styles from './Post.module.scss'
import StarSvg from '../svg/StarSvg'
import classNames from 'classnames';

const Post = ({ title = "", body = "", id, handleFavourite = () => { }, isFavourite = false }) => {
  return (
  <div className={styles.post}>
    <h2 className={styles.title}>{title}</h2>
    <p className={styles.body}>{body}</p>

    <div
       // className={`${styles.svgWrapper} ${isFavourite ? styles.active: ''}`}
      className={classNames(styles.svgWrapper, { [styles.active]: isFavourite })}

      onClick={() => { handleFavourite(id) }}
    >
      <StarSvg />
    </div>
  </div>
  )
}

Post.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
  handleFavourite: PropTypes.func,
  id: PropTypes.number.isRequired,
  isFavourite: PropTypes.bool,
}

export default Post;