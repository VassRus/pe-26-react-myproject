import './App.scss'
import { useImmer } from 'use-immer'
import { useState, useEffect } from 'react';
import axios from 'axios'
import PostsContainer from './components/PostContainer';

const App = () => {
  const URL = 'https://ajax.test-danit.com/api/json/posts';

  const [posts, setPosts] = useImmer([]);
  const [isLoading, setIsLoading] = useState(false)

  const handleFavourite = (id) => {
    setPosts( draft => {
      const post = draft.find(post => id === post.id);
      post.isFavourite = !post.isFavourite;
    })
  } 

  useEffect(function () {
    try {
      const fetchPosts = async ()=> {
        setIsLoading(true)
        const { data } = await axios.get('./posts.json');
        /*const { data } = await axios.get(URL)*/
        setPosts(data)
        console.log(data)
      }

      fetchPosts()

    } catch (err) {
      console.error(err.message)

    } finally {
      setIsLoading(false)
    }

  }, [])

  return (
    <>
    <PostsContainer posts={posts} isLoading={isLoading} handleFavourite={handleFavourite}/>
    </>
  )
}



export default App;
