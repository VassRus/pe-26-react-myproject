import { useImmer } from "use-immer";
import './App.scss'
import UserContainer from "./components/UserContainer";

function App() {
  const [users, setUsers] = useImmer([
    {
      id: '09b32085-21dd-433c-b4cc-7d6c98d1a4be',
      name: 'John',
      age: '32',
      hobby: {
        value: 'Hillel',
        isMarked: false,
      },
    },
    {
      id: '723b1f41-26a7-438c-8f04-41bbbbe5b514',
      name: 'Sam',
      age: '21',
      hobby: {
        value: 'Leo',
        isMarked: false,
      }
    },
  ])

  const markHobby = (userId) => {
    setUsers((draft) => {
      const user = draft.find(({ id }) => id === userId);
      user.hobby.isMarked = !user.hobby.isMarked;
    })
  }

  return (
    <main>
      <UserContainer users={users} markHobby={markHobby} />
    </main>
  )
}

export default App
