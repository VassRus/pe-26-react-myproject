import React from 'react';
import styles from './UserCard.module.scss';
import Hillel from "../UserContainer/111.jpg";

export default function UserCard({ name = '', age = 0, hobby = {}, id, markHobby = () => {} }) {

console.log(styles)

  return (
    <div key={id} className={styles.card}>
              <div>
                <img src='./img/leo.jpg' alt='User' width={50} />
                <img src={Hillel} alt="hillel" width={50}/>
                <h3 className={styles.title}>{name}</h3>
                <span className={styles.description}>{age}</span>
              </div>
              <ul className={styles.hobbies}>
                <li
                 /* key={id}*/
                  className={styles.hobbiesList}
                  onClick={() => markHobby(id)}
                  style={{ background: hobby?.isMarked ? 'red' : "greenyellow" }}
                >
                    {hobby.value}
                </li>
              </ul>


            </div>
  )
}
