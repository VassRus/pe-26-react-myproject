import React from 'react'
import UserCard from '../UserCard/UserCard'


export default function UserContainer({ users = [], markHobby = () => {} }) {



  return (
    <div className="user">
        <h1 className="user__title">Users</h1>

        <div className="user__container">
        {users.map(({ name, age, hobby, id }) => (
            <UserCard 
                key={id}
                name={name}
                age={age}
                hobby={hobby}
                id={id}
                markHobby={markHobby}
            />
          ))}
        </div>
      </div>
  )
}
