## Методи життєвого циклу, propTypes, defaultProps

### propTypes

Для роботи з функціоналом необхідно встановити пакет npm `prop-types`. Раніше цей функціонал був убудований у сам Реакт, зараз його винесли до окремої бібліотеки.

`propTypes` - це Runtime перевірка типів даних, які надходять до наших компонентів.

Є два способи задати `propTypes` для класового компонента:
- у вигляді статичної властивості класу `static propTypes = {...}` всередині самого класу. Такий підхід працює тільки в класових компонентах, тому краще використовувати другий варіант
- прописати після класу `Email.propTypes = {...}` - такий підхід буде працювати і в класовому, і у функціональному компоненті, тому рекомендується використовувати саме такий синтаксис

Приклад коду:

```js
import PropTypes from 'prop-types';

...

Email.propTypes = {
email: PropTypes.object.isRequired,
}
````

При розбіжності будь-якої з умов `propTypes` ми побачимо попередження в консолі.

Функціонал `propTypes` підтримує такі типи даних:
- string (рядки)
- bool (булеві вирази)
- func (функції)
- object (об'єкти)
- array (масиви)
- symbol (тип даних Symbol)
- number (числа)

Також ми можемо використовувати такі функції:
- oneOfType - всередину передається масив PropTypes, що дозволяє вказати, що властивість компонента може бути одним з декількох типів
- oneOf - всередину передається масив елементів будь-якого типу, дозволяє вказати, що значення має бути одним із наданого списку
- arrayOf - всередину передається елемент PropTypes, що дозволяє вказати, що на вхід повинен прийти масив елементів даного типу
- instanceOf - перевіряє, чи є властивість об'єктом певного класу
- shape - дозволяє вказати, які поля повинні міститися в об'єкті, що передається, та їх тип
- exact - дозволяє суворо сформувати структуру об'єкта, який має бути переданий як властивість

Ключове слово `.isRequired` дозволяє вказати, що ця властивість є обов'язковою, і без неї наш компонент не зможе нормально працювати.

### defaultProps

Функціонал `defaultProps` дозволяє вказати значення за промовчанням для властивостей, які були передані наш компонент, але які може приймати.

**ВАЖЛИВО!!!** Якщо як значення властивості передати `null`, `defaultProps` не застосовується. `defaultProps` застосовується лише якщо властивість не була передана, або ми явно передали `undefined`.

Аналогічно з `propTypes` існує два способи завдання `defaultProps` для класового компонента - у вигляді статичної властивості класу, і вказівка його після оголошення класу. Другий спосіб є кращим.

Існує угода, що `defaultProps` обов'язково треба прописувати для всіх властивостей, які не є обов'язковими в компоненті (тобто для яких `propTypes` не варто `isRequired`). Сучасні лінтери автоматично це перевіряють.

### [Життєвий цикл компонента](https://ua.reactjs.org/docs/react-component.html)

Основні методи життєвого циклу компонента: https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/

Три основні етапи – Монтування (виконується один раз), Оновлення (кожен перерендер), Розмонтування (один раз).

- `constructor()` - виконується один раз, при ініціалізації компонента
- `render()` - виводить вміст компонента на екран. Може виконуватися багато разів
- `componentDidMount()` - виконується один раз, коли компонент був вперше відрендерений. Використовується для отримання даних із сервера, ініціалізації якихось змінних чи глобальних слухачів.
- `componentDidUpdate()` - виконується щоразу, коли у компоненті щось змінюється – state або props. Використовується для повторних запитів на сервер або оновлень, коли у нас щось змінилося, але ми знаходимося в цьому компоненті (наприклад, ми знаходимося на сторінці зі списком імейлів, і в блоці пагінації натискаємо перехід на наступну сторінку)
- `componentWillUnmount()` - виконується один раз, перш ніж компонент буде видалено з екрану. Використовується для чищення якихось глобальних ресурсів. Наприклад, якщо у componentDidMount ми поставили якийсь глобальний слухач на весь наш об'єкт document, у componentWillUnmount ми повинні цей слухач прибрати, щоб не відбувалося витоків пам'яті в додатку
- `componentShouldUpdate` та функція `this.forceUpdate()` - дозволяють руками вказати, коли треба чи не треба перерендерити компонент. У 99% випадків не потрібні. Потрібно максимально намагатися уникати їх використання.

### ErrorBoundary та перехоплення помилок рендерингу в додатку

Для перехоплення помилок рендерингу використовуються методи `componendDidCatch()` та `static getDerivedStateFromError()`. Вони відловлюють помилки рендерингу в **дочірніх** компонентах.

Щоб мати можливість перехоплювати помилки у всьому додатку, зазвичай складається окремий компонент `ErrorBoundary` на самому верхньому рівні, в нього обертаються всі інші компоненти.

```js
<ErrorBoundary>
   <App />
</ErrorBoundary>
````

У режимі розробки ми все одно через секунду бачимо технічну помилку, коли вона виникає. Але це працює лише в режимі розробки, а в зібраній та задеплоєній версії користувач побачить лише зверстану сторінку з помилкою.
