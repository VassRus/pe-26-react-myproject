import { Component, useState, useEffect } from 'react';
import getNameWithTime from "./utils/getNameWithTime";
import Image from "./components/Image";
import './App.scss'
import axios from 'axios';

const App = () => {
  const [src, setSrc] = useState(null);
  const [title, setTitle] = useState('Hello');
  const [counter, setCounter] = useState(0);
  const [fact, setFact] = useState('');
  const [isLoading, setIsLoading] = useState(true);

  const url = 'https://catfact.ninja/fact';

  useEffect(() => {
    const f = async () => {
        try {
          const { data } = await axios.get(url);
          setFact(data.fact);
          setIsLoading(false)

          console.log(data)
        } catch (err) {
          console.log(err)
          setIsLoading(false)
        }
    }

    f();
  
  }, []);

  return (
    <div>
      <br />
      <h1>{ isLoading ? 'Loading......' : fact }</h1>
      <br />
      <br />
      <p>{title}</p>
      <button onClick={() => setTitle(t => t + 'o')}>Increment counter</button>
        <br />
        <br />
        <br />
        <br />

      <p>{counter}</p>
      <button onClick={() => setCounter(c => c + 1)}>Increment counter</button>


      <div>
          <button onClick={() => setSrc(`https://picsum.photos/400/200?$versio=${Math.random()}`)}>Generate image src</button>
          <button onClick={() => setSrc(null )}>Delete image src</button>
        </div>

        {src && <Image src={src} />}

        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>
        <h1>sdfs</h1>

    </div>

  )
}

// class App extends Component {

//   constructor() {
//     getNameWithTime('App', 'CONSTRUCTOR');
//     super();

//     this.state = {
//       title: '',
//       counter: 0,
//       src: null,
//     }
//   }

//   componentDidMount() {
//     getNameWithTime('App','DID MOUNT');
//   }

//   componentDidUpdate() {
//     getNameWithTime('App','DID UPDATE');
//   }

//   // componentDidCatch(error, errorInfo) {
//   //     console.log('COMPONENT DID CATCH')
//   //     console.log(error);
//   //     console.log(errorInfo);
//   // }

//   // static getDerivedStateFromError(error) {
//   //     console.log('GET DERIVED STATE FROM ERROR');
//   //     console.log(error);
//   // }

//   render(){
//     const { title, counter, src } = this.state;
//     getNameWithTime('App','RENDER');
//     return (
//       <div className="App">
//         <h1>
//           <a href="https://ru.reactjs.org/docs/react-component.html" target="_blank" rel="noreferrer">
//             { title }
//           </a>
//         </h1>

//         <p>{ counter }</p>
//         <button onClick={() => this.setState(current => ({...current, counter: current.counter + 1}))}>Increment counter</button>

//         <div>
//           <button onClick={() => this.setState({ src: `https://picsum.photos/400/200?$versio=${Math.random()}` })}>Generate image src</button>
//           <button onClick={() => this.setState({ src: null })}>Delete image src</button>
//         </div>

//         {src && <Image src={src} />}
//       </div>
//     );
//   }
// }

export default App;
