import ReactDOM from 'react-dom/client'
import { ErrorBoundary } from "react-error-boundary";
import App from './App.jsx'
import SomethingWentWrong from './components/SomethingWentWrong/SomethingWentWrong.jsx';

ReactDOM.createRoot(document.getElementById('root')).render(
    <ErrorBoundary fallback={<SomethingWentWrong />}>
        <App />
    </ErrorBoundary>,
)
