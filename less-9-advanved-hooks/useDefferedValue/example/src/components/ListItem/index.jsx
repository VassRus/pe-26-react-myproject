import PropTypes from "prop-types";
import { memo } from "react";

const ListItem = ({ text }) => <li>{text}</li>;

ListItem.propTypes = {
  text: PropTypes.string,
}

export default memo(ListItem);
