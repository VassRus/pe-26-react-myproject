import { useState, useEffect, useDeferredValue, useTransition } from 'react';
import './App.css'
import axios from 'axios';
import ListItem from './components/ListItem';


const URL = 'https://jsonplaceholder.typicode.com/todos';

function App() {

  const [todo, setTodo] = useState([]);
  const [value, setValue] = useState([]);
  const [isPending, startTransition] = useTransition()

  useEffect(() => {
    (async () => {
      try {
        const { data } = await axios.get(URL);
        setTodo([
          ...data,
        ])
      } catch (err) {
        console.log(err)
      }
    })()
  }, [])

  console.log(isPending);

  const handleChange = (e) => {
    startTransition(() => {
      setValue(e.target.value);
    })
  }



  const filteredTodo = todo.filter(({ title }) => title.includes(value))

  return (
    <>
    <div>
      <input type="text" value={value} onChange={handleChange} />
    </div>
      <ul style={{ opacity: isPending ? 0.3 : 1 }}>
        {filteredTodo.map(({ id, title }) => <ListItem key={id} text={title} />)}
      </ul>
    </>
  )
}

export default App
