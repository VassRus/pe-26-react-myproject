import './App.scss';
import Header from "./components/Header";
import GoToTop from "./components/GoToTop";
import Spacer from "./components/Spacer";
import { useInView } from "react-intersection-observer";
import { useEffect } from 'react';

function App() {

  const { ref, inView, entry } = useInView({
    threshold: 0,
  });

  console.log(inView)

  useEffect(() => {
    if (inView) {
      alert('!!!!!!!!!!!!!!')
    }
  }, [inView])

  return (
    <div className="App">
      <Header />
      <Spacer />
      <h1  ref={ref}>HELLO!!!</h1>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
      <img  src={"https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg"} alt=""/>
      <Spacer />
      <GoToTop />
    </div>
  );
}

export default App;
