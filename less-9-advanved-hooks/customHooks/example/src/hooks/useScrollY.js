import { useEffect, useState } from 'react';
import throttle from 'lodash.throttle';

const useScrollY = (timeout = 100) => {
    const [scrollY, setScrollY] = useState(0);
    const [windowHeight, setWindowHeight] = useState(0);

    useEffect(() => {
      const onScroll = throttle(() => {
        setScrollY(window.scrollY)
      }, timeout);

      setWindowHeight(window.innerHeight);
  
      window.addEventListener('scroll', onScroll);
  
      return () => {
        window.removeEventListener('scroll', onScroll);
      }
    }, [])

    return { scrollY, windowHeight };
};

export default useScrollY;