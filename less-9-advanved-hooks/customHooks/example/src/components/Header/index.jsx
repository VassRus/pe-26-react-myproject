import styles from './Header.module.scss';
import classNames from 'classnames';
import useScrollY from '../../hooks/useScrollY';


const Header= () => {
  const { scrollY } = useScrollY();

  const isShadow = scrollY >= 300;

  return (
    <header className={classNames(styles.root, { [styles.scrolled]: isShadow })}>

    </header>
  );
};

export default Header;
