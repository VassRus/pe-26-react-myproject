import styles from './GoToTop.module.scss';
import useScrollY from '../../hooks/useScrollY';

const GoToTop = () => {
  const { scrollY, windowHeight } = useScrollY(500);

  const isVisible = scrollY >= windowHeight * 2;
  
  if (!isVisible) {
    return null;
  }

  return (
    <>
      <button onClick={() => {
        window.scrollTo({
          top: 0,
          left: 0,
          behavior: "smooth",
        })
      }} className={styles.root}>UP</button>
    </>
  )
}

export default GoToTop;
