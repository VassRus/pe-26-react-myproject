import {TOGGLE_TODO, deleteTodoActionCreator, DELETE_TODO} from '../../reducers/todoReducer/todoActions';
import styles from './TodoItem.module.scss';
import classNames from 'classnames';
import PropTypes from "prop-types";



const TodoItem = ({ children, completed, dispatch = () => {}, id }) => {

    return (
        <>

            <li
                className={classNames(styles.root, {[styles.completed]: completed})}
                onClick={() => {
                    dispatch({type: TOGGLE_TODO, payload: {id}})
                }}
            >
                {children}

                {/*       <button  onClick={() => { dispatch({ type: DELETE_TODO, payload: { id } }) }}>DELETE</button>*/}
                {/* or second version through a variable*/}
                {/*   <button onClick={() => { dispatch(deleteTodoActionCreator(id)) }}>DELETE</button>*/}
                <div>
                    <button
                        onClick={() => {dispatch({type: DELETE_TODO, payload: {id}})}}>DELETE
                    </button>
                </div>

            </li>

        </>
    )
}
TodoItem.propTypes = {
    children: PropTypes.node.isRequired,
    completed: PropTypes.bool.isRequired,
    dispatch: PropTypes.func, // assuming it's a function
    id: PropTypes.string.isRequired // or whatever type your id is
};
export default TodoItem;
