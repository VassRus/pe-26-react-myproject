export const INITIAL_SET_TODO = 'INITIAL_SET_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const DELETE_TODO = 'DELETE_TODO';

export const deleteTodoActionCreator = (id) => ({ type: DELETE_TODO, payload: { id } });