import { DELETE_TODO, INITIAL_SET_TODO, TOGGLE_TODO } from "./todoActions";

const todoReducer = (state, action) => { // action -> { type: 'value', payload:  }
    switch (action.type) {
        case INITIAL_SET_TODO: {
            return action.payload;
        }

        case TOGGLE_TODO: {
            const newState = structuredClone(state);
            const { id } = action.payload;

            const index = newState.findIndex(el => el.id === id);

            if (index !== -1) {
                newState[index].completed = !newState[index].completed
                return newState;
            } else {
                return state;
            }
        }

        case DELETE_TODO: {

            const newState = structuredClone(state);
            const { id } = action.payload;
            console.log(action);

            const index = newState.findIndex(el => el.id === id);

            if (index !== -1) {
                newState.splice(index, 1);
                return newState;
            } else {
                return state;
            }
        }

        default: {
            return state;
        }
    }
}

export default todoReducer;
