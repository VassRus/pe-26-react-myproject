/*import './App.css'
import { useEffect, useState } from 'react';
import axios from 'axios';
import TodoItem from './components/TodoItem';
import useAPI from './hooks/useAPI';

const URL = 'https://jsonplaceholder.typicode.com/todos';
const URL_USER = 'https://jsonplaceholder.typicode.com/users';


function App() {

  const [todo, isLoadingTodo, isErrorTodo] = useAPI(URL);
  const [users, isLoadingUsers, isErrorUsers] = useAPI(URL_USER);

  console.log(users)

  return (
    <main>
      { isLoadingTodo && <h1>LOADING......</h1> }
      <ul>
        { todo?.map(({ id, title, completed }) => <TodoItem id={id} key={id} completed={completed}>{ title }</TodoItem>) }
      </ul>
    </main>
  )
}

export default App*/




import './App.css'
import { useEffect, useReducer } from 'react';
import axios from 'axios';
import TodoItem from './components/TodoItem';

const URL = 'https://jsonplaceholder.typicode.com/todos';
import todoReducer from './reducers/todoReducer';
import { INITIAL_SET_TODO } from './reducers/todoReducer/todoActions';




function App() {

  const [todo, dispatch] = useReducer(todoReducer, []);

  useEffect(() => {
    (async () => {
      try {
        const { data } = await axios.get(URL);
        dispatch({ type: INITIAL_SET_TODO, payload: data })
      } catch (err) {
        console.log(err);
      }
    })()
  }, [])

  console.log(todo)

  return (
    <main>
      <ul>
        { todo?.map(({ id, title, completed }) =>
            <TodoItem id={id} dispatch={dispatch} key={id} completed={completed}>{ title }</TodoItem>) }
      </ul>
    </main>
  )
}

export default App
