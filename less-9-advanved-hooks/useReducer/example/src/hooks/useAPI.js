import { useEffect, useState } from 'react';
import axios from 'axios';

const useAPI = (url) => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        (async () => {
          try {
            setIsLoading(true)
            const { data } = await axios.get(url);
            setData(data)
          } catch (err) {
            console.log(err);
            setIsError(true)
          } finally {
            setIsLoading(false)
          }
        })()
      }, []);

      return [data, isLoading, isError];
}

export default useAPI;