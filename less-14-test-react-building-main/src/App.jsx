import Typography from '@mui/material/Typography';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import { useDispatch, useSelector } from 'react-redux';

import { useEffect } from 'react';
import Header from './components/Header';
import smileIcon from './assets/emoticon-devil-outline.svg';
import styles from './App.module.scss';
import FormAdd from './components/FormAdd';
import TodoItem from './components/TodoItem';
import { fetchAllTodo } from './redux/todoSlice';

const LightTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11,
  },
}));

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllTodo());
  }, []);

  const todos = useSelector((state) => state.todos.data);

  return (
    <>
      <Header />

      <br />
      <h1>HELLO FROM THE NEW VERSION OF TODO!!!!!!!!!!!!!!!</h1>
      <h1>HELLO FROM THE NEW VERSION OF TODO!!!!!!!!!!!!!!!</h1>

      <br />

      <Typography className={styles.title} sx={{ m: 3 }} variant="h3">
        {import.meta.env.VITE_TITLE}

        <div className={styles.iconWrapper}>
          <LightTooltip className={styles.tooltip} title="Fill the form and press the button" placement="top-start">
            <img src={smileIcon} alt="emoji" width={40} height={40} />
          </LightTooltip>
        </div>
      </Typography>

      <FormAdd />

      {todos.map((todo) => <TodoItem key={todo.id} {...todo} />)}

    </>
  );
}

export default App;
