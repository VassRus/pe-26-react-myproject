import { useState } from 'react'

import './App.css'
import './myStyles.scss'

const name = 'jon';
 //const name = [1, 2, 3, 4, 5];

const isOk = true;
function App() {
    const [number, setNumber] = useState(0);
    const [string, setString] = useState('Hello');
    const [isImg, setIsImg] = useState(false);
    const src = 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcR2v8jGQFEHwDE0bEIm2Sofs-0n5RUWyiNtY_JQw46IozVB-YPU';

    const changeString = () => {
        setString( 'Nice to meet you');
    }
    const handleClick = () => {
        setNumber((number) => ++number);
        setNumber((n) => ++n);
        setNumber((n) => ++n);
        setNumber((n) => ++n);
        setNumber((n) => n +1);
    }

const showImg = ()=> {
    setIsImg(!isImg)
}

    return (
    <>
     <h1 className='title'>{name}</h1>

        <div>
            {/*використання тернарника*/}
        {isOk ? <span>OK</span> : <span>Nooooooooooo</span>}
            {/* використання логічного оператора */}
        {isOk && <p>Lorem.</p>}
      </div>

        <h1 className='title'>{number}</h1>
        <button onClick={handleClick}>Click me</button>

        <h2 onClick={changeString}>{string}</h2>


        <button onClick={showImg}>
            {!isImg ? "Show Image" : "Hide Image"}
        </button>
       {/* <button onClick={() => {setIsImg(!isImg)}}>
            {!isImg ? "Show Image" : "Hide Image"}
        </button>*/}

        {isImg && (
            <img src={src} alt="" />
        )}

    </>
  )
}

export default App
