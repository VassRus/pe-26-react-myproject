import { useState } from 'react';
import './App.css'
import './modal.scss'

function App() {

  const [isOpenModalWindow, setIsOpenModalWindow] = useState(false);


   const openModalWindow = ()=>{
    setIsOpenModalWindow(true)
   }

    const closeModalWindow = ()=>{
        setIsOpenModalWindow(false)
    }

  return (
    <>


      <button onClick={openModalWindow}>Open</button>

      <div  className='background' />

      {isOpenModalWindow && (
        <div className='modalWindow'>
          <div className='content'>
            <h1>Hello, i am a modal</h1>
              <button onClick={closeModalWindow}>Close</button>
          </div>

      </div>
      )}
    </>
  )
}

export default App
