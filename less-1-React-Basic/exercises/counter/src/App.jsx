import { useState } from 'react';
import './App.css'

function App() {

  const [counter, setCounter] = useState(0);


  const handleCounterIncrease = ()=> {
    setCounter(c => ++c)
  }

  const handleCounterDecrease = ()=> {
   // if (counter <= 0) return;

    setCounter(c => c <= 0 ? c : --c)
  }
  const resetCounter = () =>{
    setCounter(0)
  }

  return (
    <>
    <h1>{counter}</h1>
      <button onClick={handleCounterDecrease} disabled={counter === 0}>-</button>
      <button onClick={handleCounterIncrease}>+</button>
      <button onClick={resetCounter} disabled={counter === 0}>Reset</button>
     {/* <button onClick={() => {setCounter(0)}} disabled={counter === 0}>Reset</button>*/}

    </>
  )
}

export default App
