import './App.css'
import Select, { Option } from './components/Select'

function App() {

  return (
    <main>
      <h1>Hello! :)</h1>

      <select name="" id="">
        <option value="1">One</option>
        <option value="2">Two</option>
      </select>

      <Select defaultValue="FIRST">
        <Option value="1" label="FIRST" />
        <Option value="2" label="SECOND" />
        <Option value="3" label="THIRD" />
      </Select>

      <br />
      <br />
      <br />
      <br />
      <br />
      <Select defaultValue="APPLE">
        <Option value="1" label="APPLE" />
        <Option value="2" label="BANANA" />
      </Select>
    </main>
  )
}

export default App
