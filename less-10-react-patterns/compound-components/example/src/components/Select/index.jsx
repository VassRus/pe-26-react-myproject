import styles from './Select.module.scss';
import { useState, createContext, useContext } from 'react';
import classNames from 'classnames';


const SelectContext = createContext({});

const Select = ({ children, defaultValue }) => {

    const [value, setValue] = useState(defaultValue);
    const [isOpen, setIsOpen] = useState(false);

    const contectValues = {
        value,
        setValue,
        setIsOpen,
    }

    return (
        <SelectContext.Provider value={contectValues}>
            <div className={styles.root}>
                <div className={styles.selectContainer} onClick={() => { setIsOpen(prev => !prev) }}>{value}</div>

                {isOpen && (
                    <div className={styles.dropdown}>
                        {children}
                    </div>
                )}

            </div>
        </SelectContext.Provider>
    );
}

export const Option = ({ label, value }) => {
    const { setValue, value: selectValue, setIsOpen } = useContext(SelectContext);

    return (
        <div
            className={classNames(styles.option, { [styles.active]: label === selectValue })}
            onClick={() => { 
                setValue(label);
                setIsOpen(false);
             }}
        >
            {label}
        </div>
    )
}

export default Select;