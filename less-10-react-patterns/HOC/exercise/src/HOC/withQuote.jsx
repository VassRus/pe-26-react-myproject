/* eslint-disable */
import { useState, useEffect } from 'react'
import axios from 'axios';

const withQuote = (Component) => {
  return (props) => {

    const [data, setData] = useState("");

    useEffect(() => {
      async function fetchQuote() {
        try{
          const {data} = await axios.get('https://catfact.ninja/fact');
          console.log(data)
          setData(data.fact);
        }catch(err){
          console.log(err)
        }
      }
      fetchQuote();
    }, [])

    return <div>
      <Component {...props}/>
      <divquote style={{color: 'lightgrey', fontSize: 14}}>{data}</divquote>
    </div>
  }
}

export default withQuote;
