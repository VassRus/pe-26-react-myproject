import s from './User.module.css'

function User({name}) {
  
  return <div className={s.container}>
    <span>{name}</span>
  </div>
}

export default User;