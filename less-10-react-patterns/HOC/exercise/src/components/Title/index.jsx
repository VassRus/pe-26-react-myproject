import PropTypes from 'prop-types';

const Title = ({ children, color = null }) => {
  console.log('title')

  return <h1 style={{ color }}>{children}</h1>
}

Title.propTypes = {
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
}

export default Title;
