import './App.css'
import { useState, useEffect } from 'react'
import axios from 'axios';
import User from "./components/User"
import Title from './components/Title';
import withQuote from './HOC/withQuote';

const UserWithQuote = withQuote(User);
const TitleWithQuote = withQuote(Title);



function App() {

  const [data, setData] = useState([])

  useEffect(() => {
    async function fetchData() {
      try{
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/users');
        console.log(data)
        setData(data);
      }catch(err){
        console.log(err)
      }
    }

    fetchData();

  }, [])

  return (
    <main>
      <TitleWithQuote>Hello! :)</TitleWithQuote>
      <div className='container'>
        {data.map(({name, id}, index) => {
          if(index  % 2 === 0) {
            return <UserWithQuote key={id} name={name}/>
          }
          else {
           return <User key={id} name={name}/>
          }
        })}
      </div>
    </main>
  )
}



export default App
