import PropTypes from 'prop-types';

const Description = ({ children }) => {
  console.log('description')

  return (
      <p>{children}</p>
  )
}

Description.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Description;
