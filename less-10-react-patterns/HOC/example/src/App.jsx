import './App.css'
import Title from "./components/Title/index.jsx";
import Description from "./components/Description/index.jsx";
import withBackground from './HOCs/withBackground.jsx';

const TitleBackground = withBackground(Title);
const DescriptionBackground = withBackground(Description);

function App() {

  return (
    <main>
      <Title>Hello</Title>
      <Description>Here is the description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, atque.</Description>

      <TitleBackground>Title #2</TitleBackground>
      <Description>Here is the description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, atque.</Description>
      <Description>Here is the description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, atque.</Description>
      <DescriptionBackground>Hello from description</DescriptionBackground>
    </main>
  )
}

export default App
