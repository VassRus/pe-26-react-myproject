import { useState } from 'react';
import PropTypes from 'prop-types';


const Counter = ({ render = () => {} }) => {
  const [count, setCount] = useState(0);

  const incrementCount = () => setCount(count + 1);
  const decrementCount = () => setCount(count - 1);

  return (
    <div>
      { render({ count, incrementCount, decrementCount }) }
    </div>
  );
};

Counter.propTypes = {
  render: PropTypes.func,
}




// const Counter = ({ children }) => {
//   const [count, setCount] = useState(0);
//
//   const incrementCount = () => setCount(count + 1);
//   const decrementCount = () => setCount(count - 1);
//
//   return (
//     <div>
//       { typeof children === 'function' ? children({ count, incrementCount, decrementCount }) : children }
//     </div>
//   );
// };

export default Counter;
