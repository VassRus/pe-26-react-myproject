import './App.css'
import Counter from "./components/Counter/index.jsx";

function App() {

  return (
    <main>
      <h1>Hello! :)</h1>

        <Counter
        render={({ count, incrementCount , decrementCount}) => {

          return (
              <>

                  <button onClick={incrementCount}>INCREMENT</button>
                  <span style={{margin:10}}>{count}</span>
                  <button onClick={decrementCount}>DECREMENT</button>
              </>
          )

        }}
       />


        {/*     <Counter>
        { ( {count, incrementCount, decrementCount} ) => {
            return (
                <>
                    <button onClick={incrementCount}>+</button>
                    <span style={{margin: 10}}>{count}</span>
                    <button onClick={decrementCount}>-</button>
                </>
            )
        }}
      </Counter>*/}



    </main>
  )
}

export default App

