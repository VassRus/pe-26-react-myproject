import { useState, useEffect } from "react";
import axios from 'axios';

const User = ({render = () => {}}) => {

  const [data, setData] = useState({});

  useEffect(() => {
    async function fetchData() {
      try {
        const {data} = await axios.get("https://randomuser.me/api/");
        setData(data.results[0])
      }catch(err) {
        console.log(err)
      }
    }

    fetchData()
  }, [])
  return (
    <div>
      {render(data)}
    </div>
  )
}

export default User
