import './App.css'
import User from './components/User'

function App() {

  return (
    <main>
      <h1>Hello! :)</h1>

      <User render={ ({ name }) => {

        return (
            <div>
                {`${name?.title} ${name?.first} ${name?.last}`}
            </div>
        )
      }}/>

        <User
            render={ ({picture = {}, email}) => {
              const {large} = picture;
              return <div>
                        <img src={large} alt="img" />
                        <p>Email: {email}</p>
                     </div>
      }}/>
    </main>
  )
}

export default App
