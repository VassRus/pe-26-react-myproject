import React, { memo } from 'react';

const ListItem = ({children}) => {
    console.log('RENDER: ', children);
    return <li>{children}</li>;
}

export default memo(ListItem);
