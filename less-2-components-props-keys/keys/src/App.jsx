import { useState } from 'react'
import ListItem from "./components/ListItem/ListItem.jsx";
import './App.css'

function App() {
  const [items, setItems] = useState([
    '0.9725067665779998',
    '0.5223571231487212',
    '0.8613543409506943',
    '0.49496115828960896',
    '0.31147916020306465',
    '0.9118810877216494',
  ])

  const updateState = () => {
    //setItems(state => ([`${Math.random()}`, ...state]))
     setItems(state => ([...state, `${Math.random()}`])) // Все +- ОК
  }

  return (
    <div className="App">
      <div style={{ padding: '40px 10px' }}>
        <button onClick={updateState}>Update state</button>
        <ul>
          {items.map((item) => (
            <div key={item}>
              <ListItem>{item}</ListItem>
            </div>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default App
