
import './App.css'
import Button from "./components/Button/index.jsx";
import {useState} from "react";
import ButtonContainer from "./components/ButtonContainer/index.jsx";
import Counter from "./components/Counter/index.jsx";

function App() {

    // const [count, setCount] = useState(0);

     const arr = ['John', 'Bob', 'Sam'];

/*    // const [user, setUser] = useState({
    //   name: 'John',
    //   data: {
    //     age: 30
    //   }
    // });
    // const incrementAge = () => {
    //   setUser((user) => {
    //    const newUser = structuredClone(user);
    //    newUser.data.age = newUser.data.age + 1;
    //    return newUser;
    //   // return {...user, data: {...user.data, age: user.data.age + 1}}
    //   })
    // }
    */

    /*    const [data, setData] = useState(['john', 'sam', 'bob']);

    const deleteUser = (userName) => {

        setData(u => {
            const newUser = structuredClone(u);

            // const index = newUser.indexOf(userName);
            // newUser.splice(index, 1);

            // return newUser;

            return newUser.filter(el => el !== userName)
        })

    }*/

    const [count, setCount] = useState(0);

  return (
    <section>


        {arr.map((elem, index)=>(
            <div>
                <h2> user {index} </h2>
                <button onClick={()=> {alert(`hello ${elem}`)}}>{elem}</button>
            </div>
            ))}

        <br/>
        <br/>
        <Counter count={count}/>
        <ButtonContainer count={count}  setCount={setCount}   />
        <br/>
        <br/>


       <Button
        color="yellow"
        onClick={()=>{alert('hello button 1')}}
       >
            <h1> button  </h1>   {/*це children */}
            <p>loren </p>    {/* це children */}

       </Button>

        {/* <div>
        <h4>{user.name}</h4>
        <h6>{user.data.age}</h6>
      </div>

      <button onClick={incrementAge}>+ age</button> */}


     {/*   { data.map(user => <p onClick={() => deleteUser(user)} key={user}>{user}</p>) }*/}

    </section >
  )
}

export default App
