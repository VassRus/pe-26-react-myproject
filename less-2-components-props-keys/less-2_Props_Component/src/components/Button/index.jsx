import PropTypes from 'prop-types';

const Button = ({ color = 'green', onClick = () => {}, children }) => {

    return (
        <button
            style={{ background: color }}
            onClick={onClick}
        >
            { children }
        </button>
    );
};

// Index.defaultProps = {
//     color: 'green',
//     onClick: () => { console.log(1) },
// };

Button.propTypes = {
    color: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}
export default Button;
