import Button from "../Button/index.jsx";
import Counter from "../Counter/index.jsx";


const ButtonContainer = ({ count, setCount  }) => {

    const increment = () => {
        setCount(count => count + 1);
    }

    const decrement = () => {
         /*if(count<=0) return;*/
        setCount(count => count <=0 ? count : --count);
    }

    return (
        <div>
            <Counter count={count}/>
            <Button onClick={increment}  color="pink" > + </Button>
            <Button onClick={()=>{setCount(0)}}  color="red">Reset</Button>
            <Button onClick={decrement}  color="pink" > - </Button>
<br/>
            <Button>Empty</Button>

        </div>
    )
}

export default ButtonContainer;
