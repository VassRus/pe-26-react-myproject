import './App.scss';
import Header from "./components/Header/index.jsx";
import Post from "./components/Post/index.jsx";
import { useState } from "react";


const initialsEmails = [
  {
    isRead: false,
    id: Math.random(),
    from: 'The Postman Team',
    topic: 'Announcing Postman’s New Plans and Pricing',
    body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.'
  },
  {
    isRead: false,
    id: Math.random(),
    from: 'Djinni',
    topic: 'JavaScript вакансії за вашим профілем',
    body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 4,6 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.'
  },
  {
    isRead: false,
    id: Math.random(),
    from: 'Natalia Pemchyshyn',
    topic: 'JavaScript Developer at GlobalNogic',
    body: 'Привіт!Я рекрутер компанії GlobalNogic. Ми зараз у пошуках Lead/Senior React Developer (фултайм/ремоут) на фармацевтичний проект. Розробляємо девайс, який моніторить стан здоров\'я свійських тварин.Стек: React, Ionic, HTML, CSS, RxJs, Java. Поспілкуємось?'
  },
]

const userData = {
  avatar: 'https://i.pravatar.cc/80',
  name: 'John',
}

const title = 'Components and Props';


const App = () => {
  // your code here
  const [emails, setEmails] = useState(initialsEmails);
  const newEmailsNumber = () => {
    let num = 0;
    emails.forEach( email => {
      if(!email.isRead) num++
    })
    return num;
  }

  const handleIsDone = (id) => {
    setEmails(emails => {
      const newEmails = structuredClone(emails);

      const index = newEmails.findIndex(el => el.id === id);
      newEmails[index].isRead = !newEmails[index].isRead;

      return newEmails;

       // return newEmails.map(email => email.id === id ? {...email, isRead: !email.isRead} : email)
    })
  }

      return (
    <>
      <Header
          title={title}
          userData={userData}
          newEmailsNumber={newEmailsNumber()}
      />
      {/*<Header title={title} avatar={userData.avatar} name={userData.name}/>*/}


      <main>
        <div className='emails'>
          <h1>EMAILS</h1>
        </div>
        {emails.map(email =>
            <Post
                key={email.id}
                email={email}
                handleIsDone={handleIsDone}
            />)}

      </main>



      <footer>
        <span className='footer__title'>{ title }</span>
        <span className='footer__copyright'>{new Date().getFullYear()}, FE-17-ONLINE</span>
      </footer>
    </>
  );
}

export default App;
