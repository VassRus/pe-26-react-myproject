import PropTypes from 'prop-types'

const Post = ({email, handleIsDone}) => {

    return (
        <div style={email.isRead ? {backgroundColor: 'orangered'} : {}} className="email__container" onClick={() => handleIsDone(email.id)} >
            <span className="email__from">{ email.from }</span>
            <h3 className="email__topic">{ email.topic }</h3>
            <p className="email__body">{ email.body }</p>
        </div>
    )
}

Post.propTypes = {
    email: PropTypes.object,
    onClick: PropTypes.func,
    handleIsDone:PropTypes.func
}

export default Post;
