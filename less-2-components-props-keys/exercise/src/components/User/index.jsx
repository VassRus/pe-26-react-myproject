import PropTypes from "prop-types";


const User =({userData={}})=>{


    return(
        <>
            <div className='header__user-container'>
                <img src={userData.avatar} alt={userData.name} />
                <span>{userData.name}</span>
            </div>
        </>
    )
}
User.propTypes = {
    userData: PropTypes.shape({
        avatar: PropTypes.string,
        name: PropTypes.string,
    }),
}


export default User