import PropTypes from "prop-types";
import Nav from "../Nav/index.jsx";
import User from "../User/index.jsx";
const Header = ({title , userData = {}, newEmailsNumber})=>{

    return(

            <header>
                <span className='header__title'>{ title }</span>

                <Nav/>

                <span style={{marginLeft: 'auto', marginRight: '10px'}}>New Emails: {newEmailsNumber}</span>

                <User userData={userData}/>

            </header>


    )
}
Header.propTypes = {
    title: PropTypes.string.isRequired, // title should be a string and required
    userData: PropTypes.object, // userData should be an object
    newEmailsNumber: PropTypes.number.isRequired, // newEmailsNumber should be a number and required
};

// Header.propTypes = {
//     title: PropTypes.string.isRequired,
//     userData: PropTypes.shape({
//         avatar: PropTypes.string,
//         name: PropTypes.string,
//     }),
// }

export default Header