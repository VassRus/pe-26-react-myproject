const {users, token} = require('../../data');
const {sendBadResponse, sendGoodResponse} = require("../utils");

const editProfile = (app) => {
    app.put('/edit-profile', (req, res) => {
        const reqToken = req.header('Authorization');
        if (!reqToken) {
            sendBadResponse(res, 401, `Authorization header is required`);
            return;
        }

        const clearedReqToken = reqToken.replace(/Bearer\s/gi, '').trim();
        if (clearedReqToken !== token) {
            sendBadResponse(res, 401, `Authorization token is incorrect`);
            return;
        }

        if (!req.body) {
            sendBadResponse(res, 400, `Body is required.`);
            return;
        }

        const index = users.findIndex(({id}) => id === req.body?.id);
        if (index === -1) {
            sendBadResponse(res, 404, `User with such id not found`);
            return;
        }

        const {name, age, city} = req.body;
        users[index] = {...users[index], name, age, city} // eslint-disable-line

        sendGoodResponse(res, {user: users[index]});
    })
}
module.exports = editProfile;