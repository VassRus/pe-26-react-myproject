const {users, token} = require('../../data');
const {v4: getId} = require('uuid');
const {sendBadResponse, sendGoodResponse} = require("../utils");
const {emailRegExp} = require("../../constants/regExp");

const signUp = (app) => {
    app.post('/sign-up', (req, res) => {
        if (!req.body) {
            sendBadResponse(res, 400, `Body is required.`);
            return;
        }

        if (!req.body?.email) {
            sendBadResponse(res, 400, `Email is required.`);
            return;
        }

        if (!emailRegExp.test(req.body?.email)) {
            sendBadResponse(res, 400, `Invalid format of email`);
            return;
        }

        if (!req.body?.password || !req.body?.repeatPassword) {
            sendBadResponse(res, 400, `Password is required.`);
            return;
        }

        if (req.body?.password !== req.body?.repeatPassword) {
            sendBadResponse(res, 400, `Password and repeatPassword mismatch.`);
            return;
        }

        if (users.find(({email}) => email === req.body?.email)) {
            sendBadResponse(res, 400, `User with email ${req.body?.email} already exist`);
            return;
        }

        const {name, age, city, email, password} = req.body;
        const newUser = {
            name,
            age,
            city,
            email,
            id: getId(),
        }

        users.push({...newUser, password}); // eslint-disable-line
        sendGoodResponse(res, {user: newUser, token});
    })
}
module.exports = signUp;