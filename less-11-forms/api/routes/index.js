const signUp = require('./sign-up');
const signIn = require('./sign-in');
const editProfile = require('./edit-profile');

module.exports = (app) => {
    signUp(app);
    signIn(app);
    editProfile(app);
}

