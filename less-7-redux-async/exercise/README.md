1API endpoints:

- Get Items: GET http://localhost:5001/items

- Get Favourites: GET http://localhost:5001/favourites
- Add Favourite: POST http://localhost:5001/favourites (body: {id: 1})
- Delete Favourite: DELETE http://localhost:5001/favourites/1

- Get Cart: GET http://localhost:5001/cart
- Add to Cart: POST http://localhost:5001/cart (body: {id: 1})
- Decrement quantity: PATCH http://localhost:5001/cart/1
- Delete from Cart: DELETE http://localhost:5001/cart/1
