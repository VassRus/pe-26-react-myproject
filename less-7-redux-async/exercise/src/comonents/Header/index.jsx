import styles from './Header.module.scss';
import Nav from "../Nav/index.jsx";
import { updateA } from '../../redux/productsSlice.js';

import { useSelector, useDispatch } from 'react-redux';

const Header = () => {

  const favouritesCount = useSelector(state => state.favourites.data.length);
  const a = useSelector(state => state.products.a)
  const dispatch = useDispatch();
  

    return (
        <header className={styles.header}>
            <span className={styles.logo}>Logo</span>
            <Nav />

          <div className={styles.quantityWrapper}>
            <span>A: {a}</span>
            <button onClick={() => dispatch(updateA())}>update a</button>
            <span>FavouritesCount: {favouritesCount}</span>
            <span>C: 0</span>
          </div>
        </header>
    )
}

export default Header;
