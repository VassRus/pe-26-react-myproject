import PropTypes from "prop-types";
import styles from './Item.module.scss';
import CartIcon from '../../assets/svg/CartSVG';
import FavouriteIcon from '../../assets/svg/StarSVG';
import classNames from "classnames";

import {useDispatch} from 'react-redux';
import { addToFavourites, deleteFromFavourites } from "../../redux/favouritesSlice";

const Item = ({ id, name, price, description, img, isFavourite }) => {

  const dispatch = useDispatch();
  function handleFavourites() {
    if(isFavourite) {
      dispatch(deleteFromFavourites(id))  
    } else {
      dispatch(addToFavourites(id))
    }
  }

  return (
    <div className={styles.root}>
      <div className={styles.imgContainer}>
        <img src={img} alt={name} />
      </div>
      <div className={styles.contentWrapper}>
        <div className={styles.info}>
          <div className={styles.name}>{name}</div>
          <div className={styles.price}>{price}$</div>
          <div className={styles.description}>{description}</div>
        </div>

        <div className={styles.btnContainer}>
          <button className={classNames(styles.btn, styles.favouriteBtn, { [styles.isFavourite]: isFavourite })} onClick={handleFavourites}><FavouriteIcon /></button>
          <button className={classNames(styles.btn, styles.cartBtn)}><CartIcon /></button>
        </div>
      </div>

    </div>
  )
}

Item.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  description: PropTypes.string,
  img: PropTypes.string,
  isFavourite: PropTypes.bool,
};

Item.defaultProps = {
  id: '',
  name: '',
  price: 0,
  description: '',
  img: '',
  isFavourite: false,
}

export default Item;
