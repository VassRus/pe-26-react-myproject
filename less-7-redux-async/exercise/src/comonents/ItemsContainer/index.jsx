import styles from './ItemsContainer.module.scss';
import Item from "../Item/index.jsx";
import {useSelector} from "react-redux";

const ItemsContainer = ({ isLoading = false, isError = false, items = [] }) => {
 /* щоб перевикористовувати компоненти ItemsContainer у FavouritePage потрібно переписасти/деструктуризувати пропси ось так як вище а це що нижче закинути у FavouritePage */
 /*  const items = useSelector(state => state.products.data);
  const isLoading = useSelector(state => state.products.isLoading);
  const isError = useSelector(state => state.products.isError);*/

  if (isLoading) {
    return (
      <div className={styles.root}>
        <h1>Loading......</h1>
      </div>
    )
  }

  if (isError) {
    return (
      <div className={styles.root}>
        <h1>Sorry, we are broken :(</h1>
        <button style={{ fontSize: 30}} onClick={() => { window.location.reload() }}>Click to reload</button>
      </div>
    )
  }

  return (

    <div className={styles.root}>
      {items.map(item => <Item key={item.id} {...item} />)}
    </div>
  )
}
export default ItemsContainer;
