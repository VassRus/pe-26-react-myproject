import styles from './ItemPlaceholder.module.scss';

const ItemPlaceholder = () => <div className={styles.placeholder} />;

export default ItemPlaceholder;