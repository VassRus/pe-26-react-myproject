import styles from './Nav.module.scss';
import { NavLink } from "react-router-dom";
import classNames from "classnames";

const Nav = () => {
    return (
        <nav className={styles.nav}>
            <ul>
              <li>
                <NavLink className={({ isActive }) => classNames(styles.link, { [styles.active]: isActive })} to='/' end>Home</NavLink>
              </li>

              <li>
                <NavLink className={({ isActive }) => classNames(styles.link, { [styles.active]: isActive })} to='/favourites' end>Favourites</NavLink>
              </li>

              <li>
                <NavLink className={({ isActive }) => classNames(styles.link, { [styles.active]: isActive })} to='/cart' end>Cart</NavLink>
              </li>
            </ul>
        </nav>
    )
}

export default Nav;