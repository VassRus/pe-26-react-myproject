import { useSelector } from "react-redux";

import ItemsContainer from "../../comonents/ItemsContainer";

const FavouritePage = () => {

  const items = useSelector(state => state.favourites.data);
  const isLoading = useSelector(state => state.favourites.isLoading);
  const isError = useSelector(state => state.favourites.isError);

  return (
    <div>
      <h1>Favourites</h1>
      
      <ItemsContainer items={items} isLoading={isLoading} isError={isError}/>
    </div>
  )
}

export default FavouritePage;