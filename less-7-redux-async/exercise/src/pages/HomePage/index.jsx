import { createSelector } from "@reduxjs/toolkit";
import ItemsContainer from "../../comonents/ItemsContainer/index.jsx";
import { useSelector } from "react-redux";
const HomePage = () => {
    /*const items = useSelector(state => state.products.data);*/
    const isLoading = useSelector(state => state.products.isLoading);
    const isError = useSelector(state => state.products.isError);

// =====  фунція реселект -  createSelector ( ) яка не дає рендирити всі елементи на сторінці =====
  const selectAllProductsMemoized = createSelector(
    state => state.products.data,
    state => state.favourites.data,
    (products, favourites) => {

      const itemsToRender = products.map(product => {
        const index = favourites.findIndex(el => el.id === product.id);
        if (index !== -1) {
          return { ...product, isFavourite: true }
        }
        return product;
      })
      return itemsToRender;
    }
  )
  const items = useSelector(selectAllProductsMemoized);

  return (
    <div>
      <h1>Home</h1>
      <ItemsContainer items={items} isLoading={isLoading} isError={isError} />
    </div>
  )
}
export default HomePage;
