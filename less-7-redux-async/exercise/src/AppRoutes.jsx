import { Route, Routes } from 'react-router-dom';

import HomePage from './pages/HomePage/index.jsx';
import FavouritePage from "./pages/FavouritePage/index.jsx";
import CartPage from "./pages/CartPage/index.jsx";


function AppRoutes() {


  return (
    <Routes>
      <Route path='/' element={<HomePage />} />
      <Route path='/favourites' element={<FavouritePage />} />
      <Route path='/cart' element={<CartPage />} />

      <Route path="*" element={<h1>404 - PAGE NO FOUND</h1>} />
    </Routes>

  )
}

export default AppRoutes;
