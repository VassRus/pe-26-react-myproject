import './App.css'
import Header from "./comonents/Header";
import AppRoutes from "./AppRoutes";
import { useDispatch } from 'react-redux';
import { fetchAllProducts } from './redux/productsSlice';
import { fetchAllFavourites } from './redux/favouritesSlice';

function App() {
  const dispatch = useDispatch();
  dispatch(fetchAllProducts());
  dispatch(fetchAllFavourites())



  return (
    <div className="App">
      <Header />
   
      <section>
        <AppRoutes />
      </section>
    </div>
  )
}

export default App
