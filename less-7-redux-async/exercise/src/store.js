import { configureStore } from '@reduxjs/toolkit'
import productsSlice from './redux/productsSlice'
import favouritesSlice from './redux/favouritesSlice'
// import logger from 'redux-logger'

export default configureStore({
  reducer: {
    products:  productsSlice,
    favourites: favouritesSlice,
  },
  // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
})