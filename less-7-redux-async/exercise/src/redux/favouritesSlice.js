import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchAllFavourites = createAsyncThunk(
  'favourites/fetchAllFavourites',
  async () => {
    const { data } = await axios.get('http://localhost:5001/favourites');
    return data;
  }
);
export const addToFavourites = createAsyncThunk(
  'favourites/addToFavourites',
  async (id) => {
    const { data } = await axios.post('http://localhost:5001/favourites', {id});
    return data;
  }
)
export const deleteFromFavourites = createAsyncThunk(
  'favourites/deleteFromFavourites',
  async (id) => {
    const { data } = await axios.delete(`http://localhost:5001/favourites/${id}`);
    return data;
  }
)

const initialState = {
  data: [],
  isLoading: false,
  isError: false,
};

export const favouritesSlice = createSlice({
  name: 'favourites',
  initialState,
  reducers: {},

  extraReducers: (builder) => {

    // FETCH ALL ---------------------------------------------------------------------------
    builder.addCase(fetchAllFavourites.fulfilled, (state, action) => {
      state.data = action.payload.map(el =>({ ...el, isFavourite: true }));
      state.isLoading = false;
    })

    builder.addCase(fetchAllFavourites.pending, (state) => {
      state.isLoading = true;
    })

    builder.addCase(fetchAllFavourites.rejected, (state) => {
      state.isLoading = false;
      state.isError = true;
    })




    // ADD TO FAV ---------------------------------------------------------------------------
    builder.addCase(addToFavourites.fulfilled, (state, action) => {
      state.data.push({...action.payload.item, isFavourite: true});
      state.isLoading = false;
    })
    builder.addCase(addToFavourites.pending, (state) => {
        state.isLoading = true;
    })
    builder.addCase(addToFavourites.rejected, (state) => {
        state.isLoading = false;
        state.isError = true;
    })
    // DELETE TO FAV ---------------------------------------------------------------------------
    builder.addCase(deleteFromFavourites.fulfilled, (state, action) => {
     // console.log(action.payload.item)
      const index = state.data.findIndex(todo => todo.id === action.payload.item.id)
      if (index !== -1) state.data.splice(index, 1)
    })

    // builder.addCase(deleteFromFavourites.pending, (state) => {
    //     state.isLoading = true;
    // })

    // builder.addCase(deleteFromFavourites.rejected, (state) => {
    //     state.isLoading = false;
    //     state.isError = true;
    // })
  }
})
// export const {} = productsSlice.actions
export default favouritesSlice.reducer