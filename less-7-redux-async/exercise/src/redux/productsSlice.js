import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from "axios";

export const fetchAllProducts = createAsyncThunk(
    'products/fetchAllProducts',
    async () => {
        const { data } = await axios.get('http://localhost:5001/items');
        return data;
    }
);

const initialState = {
    data: [],
    isLoading: false,
    isError: false,
    a: 1,
};

export const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        updateA: (state) => {
            state.a++;
        }
    },

    extraReducers: (builder) => {

        builder.addCase(fetchAllProducts.fulfilled, (state, action) => {
            state.data = action.payload;
            state.isLoading = false;
        })
        builder.addCase(fetchAllProducts.pending, (state) => {
            state.isLoading = true;
        })
        builder.addCase(fetchAllProducts.rejected, (state, action) => {
            console.log(action)
            state.isLoading = false;
            state.isError = true;
        })
    }
})
export const {updateA} = productsSlice.actions
export default productsSlice.reducer