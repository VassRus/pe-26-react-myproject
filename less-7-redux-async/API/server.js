import app from './app.js';

app.listen(5001, () => {
    console.log('Server listening on http://localhost:5001');
});