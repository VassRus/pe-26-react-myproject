import { Router } from "express";
import CartController from "../controllers/CartController.js";

const cartRouter = Router();
const cartController = new CartController();

cartRouter.get('/', cartController.getCart);
cartRouter.post('/', cartController.addToCart);
cartRouter.delete('/:id', cartController.removeFromCart);
cartRouter.patch('/:id', cartController.decrementQuantity);

export default cartRouter;