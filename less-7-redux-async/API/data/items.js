import { v4 as uuidv4 } from "uuid";

const items = [
  {
    id: uuidv4(),
    name: 'Item 1',
    price: 100,
    description: 'This is item 1. It\'s very cool, buy it!',
    img: 'http://localhost:5001/images/1.jpg',
  },
  {
    id: uuidv4(),
    name: 'Item 2',
    price: 200,
    description: 'This is item 2. It\'s very cool, buy it! You will not regret it! It\'s your best choice!',
    img: 'http://localhost:5001/images/2.jpg',
  },
  {
    id: uuidv4(),
    name: 'Item 3',
    price: 300,
    description: 'This is item 3',
    img: 'http://localhost:5001/images/3.jpg',
  },
  {
    id: uuidv4(),
    name: 'Item 4',
    price: 400,
    description: 'This is item 4. It\'s very cool, buy it! You will not regret it! It\'s your best choice! My friend, buy it! Come on! Do it! Don\'t think! Just do it!',
    img: 'http://localhost:5001/images/4.jpg',
  },
  {
    id: uuidv4(),
    name: 'Item 5',
    price: 500,
    description: 'This is item 5',
    img: 'http://localhost:5001/images/5.jpg',
  },
  {
    id: uuidv4(),
    name: 'Item 6',
    price: 600,
    description: 'This is item 6. It\'s very cool, buy it! You will not regret it! It\'s your best choice! My friend, buy it! Come on! Do it! Don\'t think! Just do it!',
    img: 'http://localhost:5001/images/6.jpg',
  },
];

export default items;