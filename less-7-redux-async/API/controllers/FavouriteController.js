import items from "../data/items.js";

class FavouriteController {
  favourites = [];
  timeout = 0;

  getFavourites = (req, res) => {
    setTimeout(() => {
      res.json(this.favourites);
    }, this.timeout)
  }

  addToFavourites = (req, res) => {
    setTimeout(() => {
      const { id } = req.body;

      if (!id) {
        return res.status(400).json({ message: 'Id is required' });
      }

      console.log(id)
      console.log(items)

      const item = items.find(item => item.id === id);

      if (!item) {
        return res.status(404).json({ message: 'Item not found' });
      }

      if (this.favourites.find(favourite => favourite.id === item.id)) {
        return res.status(400).json({ message: 'Item already in favourites' });
      }

      this.favourites.push(item);
      res.json({ message: 'Item added', item });
    }, this.timeout)
  }

  removeFromFavourites = (req, res) => {
    setTimeout(() => {
      const { id } = req.params;

      if (!id) {
        return res.status(400).json({ message: 'Id is required' });
      }

      const itemIndex = this.favourites.findIndex(item => item.id === id);

      if (itemIndex === -1) {
        return res.status(404).json({ message: 'Item not found' });
      }

      const item = this.favourites.splice(itemIndex, 1);
      res.json({ message: 'Item removed', item: item[0] });
    }, this.timeout)
  }
}

export default FavouriteController;
