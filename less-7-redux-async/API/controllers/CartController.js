import items from "../data/items.js";

class CartController {
  cart = [];
  timeout = 0;

  getCart = (req, res) => {
    setTimeout(() => {
      res.json(this.cart);
    }, this.timeout);
  }

  addToCart = (req, res) => {
    setTimeout(() => {
      const { id } = req.body;

      if (!id) {
        return res.status(400).json({ message: 'Id is required' });
      }

      const item = items.find(item => item.id === id);

      if (!item) {
        return res.status(404).json({ message: 'Item not found' });
      }

      const itemIndex = this.cart.findIndex(item => item.id === id);

      if (itemIndex !== -1) {
        this.cart[itemIndex].quantity++;
        res.json(this.cart[itemIndex]);
      } else {
        this.cart.push({ ...item, quantity: 1 });
        res.json(this.cart.find(item => item.id === id));
      }
    }, this.timeout);
  }

  removeFromCart = (req, res) => {
    setTimeout(() => {
      const { id } = req.params;

      if (!id) {
        return res.status(400).json({ message: 'Id is required' });
      }

      const itemIndex = this.cart.findIndex(item => item.id === id);

      if (itemIndex !== -1) {
        this.cart.splice(itemIndex, 1);
        res.json({ message: 'Item removed' });
      } else {
        res.status(404).json({ message: 'Item not found' });
      }
    }, this.timeout);
  }

  decrementQuantity = (req, res) => {
    setTimeout(() => {
      const { id } = req.params;

      if (!id) {
        return res.status(400).json({ message: 'Id is required' });
      }

      const itemIndex = this.cart.findIndex(item => item.id === id);

      if (itemIndex !== -1) {
        if (this.cart[itemIndex].quantity === 1) {
          this.cart.splice(itemIndex, 1);
          res.json({ message: 'Item removed' });
        } else {
          this.cart[itemIndex].quantity--;
          res.json(this.cart[itemIndex]);
        }
      } else {
        res.status(404).json({ message: 'Item not found' });
      }
    }, this.timeout);
  }
}

export default CartController;