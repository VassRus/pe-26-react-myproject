import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

const LoginRoute = ({ setUser = () => {} }) => {

  const navigate = useNavigate(); // hook

  const onSubmit = async (e) => {
    e.preventDefault();

    try {
      const { data } = await axios.get('./user.json');
      localStorage.setItem('user', JSON.stringify(data));
      setUser(data);
      navigate('/')
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <>
      <h1>Login</h1>

      <form action="" onSubmit={onSubmit}>
        <input type="text" placeholder="name" />
        <input type="password" placeholder="password" />

        <button type="submit">Submit</button>
      </form>
    </>
  )
}

LoginRoute.propTypes = {
  setUser: PropTypes.func,
};

export default LoginRoute;
