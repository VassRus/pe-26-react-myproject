import PropTypes from 'prop-types';
import ProductsContainer from '../../components/ProductsContainer'

const HomeRoute = ({ products = [], handleFavourite =()=>{}, addToFavorite=()=>{}, addToCart = () => {} }) => {

  return (
    <>
      <h1>PRODUCTS</h1>
      <ProductsContainer products={products}  addToCart={addToCart} handleFavourite={handleFavourite} addToFavorite={addToFavorite}/>
    </>
  )
}

HomeRoute.propTypes = {
  products: PropTypes.array,
  addToCart: PropTypes.func,
  addToFavorite: PropTypes.func,
    handleFavourite: PropTypes.func,
};

export default HomeRoute;
