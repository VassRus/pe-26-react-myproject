import PropTypes from 'prop-types';
import CartItem from '../../components/CartItem';

const CartRoute = ({ 
  cart = [],
  decrementCartItem = () => {},
  incrementCartItem = () => {},
    deleteCartItem = () => {},
 }) => {

  const totalPrice = cart.reduce((acc, el) => acc + el.price * el.count, 0)

  return (
    <>
      <h1>Cart</h1>

      <div>
        {cart.map(item =>
        <CartItem
            key={item.id}
            item={item}
            decrementCartItem={decrementCartItem}
            incrementCartItem={incrementCartItem}
            deleteCartItem={deleteCartItem}
        />)}
      </div>

      <br />
      <br />

      <h2>TOTAL PRICE: { totalPrice } USD </h2>

      <br />
      <br />

    </>
  )
}

CartRoute.propTypes = {
  cart: PropTypes.array,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
};

export default CartRoute;
