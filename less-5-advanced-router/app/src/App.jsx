import { useEffect, useState } from "react";
import axios from 'axios';
import Header from "./components/Header";
import AppRouter from "./AppRouter.jsx";
import { useImmer } from "use-immer";
import { getCartFromLS, getFavoriteFromLS, setCartToLS, setFavoriteToLS } from "./utils/cartLocalStorageHelpers.js";
import {useMemo, useCallback} from "react";

function App() {
  const [products, setProducts] = useImmer([]);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')));
  const [cart, setCart] = useImmer([]);
  const [favorite, setFavorite] = useImmer([]);
 /* const [isFavourite, setIsFavourite] = useState([]);*/

  const cartTotalCount = cart?.reduce((acc, el) => acc + el.count || 0 , 0)
  const favoriteTotalCount = favorite?.reduce((acc, el) => acc + el.count || 0 , 0)


  const addToCart = useCallback((product)  => {
    setCart((draft => {
      const cartItem = draft?.find(({ id }) => id === product.id);
      if (!cartItem) {
        draft?.push({...product, count: 1});
      } else {
        cartItem.count++;
      }
      setCartToLS(draft);
    }))
  },[setCart])


  const addToFavorite = (favorite) => {
    setFavorite((draft => {
          const favoriteItem = draft?.find(({ id }) => id === favorite.id);
          if (!favoriteItem) {
            draft?.push({...favorite, count: 1});
          } else if (favoriteItem.count > 0){
            favoriteItem.count--;
          } else {
              favoriteItem.count++;
          }
          setFavoriteToLS(draft)
        }
    ))
  }

  const decrementCartItem = (itemId) => {
    setCart(draft => {
      const cartItem = draft.find(({ id }) => id === itemId);
      if (cartItem.count > 1) {
        cartItem.count--;
        setCartToLS(draft);
      }
    })
  }

  const incrementCartItem = (itemId) => {
    setCart(draft => {
      const cartItem = draft.find(({ id }) => id === itemId);
      cartItem.count++;
      setCartToLS(draft);
    })
  }

  const deleteCartItem = (itemId) => {
    setCart(draft => {
      const updatedCart = draft.filter(item => item.id !== itemId);
      setCartToLS(updatedCart);
      return updatedCart;
    });
  };

  // useEffect(() => {
  //   if (cart?.length !== 0) {
  //     setCartToLS(cart);
  //   }
  // }, [cart])


  // READ CART STATE FROM LOCAL STORAGE --------------------------------------------------
  useEffect(() => {
      setCart(getCartFromLS());
  }, [])

  useEffect(() => {
    setFavorite(getFavoriteFromLS());
  }, [])


  const handleFavourite = (id) => {
    setProducts( draft => {
      const product = draft.find(product  => id === product.id);
      product.isFavourite = !product.isFavourite;
    })
  }

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const { data } = await axios.get('./data.json');
        setProducts(data);
      } catch (err) {
        console.log(err);
      }
    }

    fetchProducts();
  }, []);

const memoizedProducts = useMemo(()=> products,[products])

  return (
    <>
      <Header cartTotalCount={cartTotalCount} favoriteTotalCount={favoriteTotalCount}/>

      <main>
        <AppRouter
          products={memoizedProducts}
          user={user}
          setUser={setUser}
          addToCart={addToCart}
          addToFavorite={addToFavorite}
          handleFavourite={handleFavourite}
          cart={cart}
          decrementCartItem={decrementCartItem}
          incrementCartItem={incrementCartItem}
          deleteCartItem={deleteCartItem}
        />
      </main>
    </>
  )
}

export default App
