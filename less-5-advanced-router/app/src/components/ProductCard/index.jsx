import PropTypes from 'prop-types';
import styles from './ProductCard.module.scss';
import Favorite from "../svg/Favorite/index.jsx";
import classNames from 'classnames';
import {memo} from "react";

function ProductCard({ product = {} , isFavourite = false , addToFavorite= () => {},  handleFavourite= () => {}, addToCart = () => {} }) {
  //  const{id,name,image,shortDescription,price,isFavourite } = product
  //  console.log(product)

    return (

    <div className={styles.productCard}>
      <img src={product.image} alt={product.name} />
      <h2 className={styles.productName}>{product.name}</h2>
      <p className={styles.productDescription}>{product.shortDescription}</p>
      <p className={styles.productPrice}>Price: {product.price}</p>

      <button onClick={() => {addToCart(product)}} className={styles.addToCartBtn}>Add To Cart</button>

        <div
            className={classNames(styles.svgWrapper, {[styles.active]: isFavourite})}
           // className={`${styles.svgWrapper} ${isFavourite ? styles.active: ''}`}
            onClick={() => {addToFavorite(product)}}
                   // onClick={() => {handleFavourite(product.id)}}
            >
            <div  onClick={() => {handleFavourite(product.id)}}>
                <Favorite   />
            </div>


            {/*  <Favorite  {isFavourite ? styles.active: '' } />
            {isFavourite && <Favorite />}
            {!isFavourite && <Favorite />}*/}
        </div>


    </div>
  );
}



ProductCard.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string,
        shortDescription: PropTypes.string,
        image: PropTypes.string,
        price: PropTypes.number,

    }),
    isFavourite: PropTypes.bool,
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func,
    handleFavourite: PropTypes.func,
};

export default memo(ProductCard);
