import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';
import styles from './ProductsContainer.module.scss';

const ProductsContainer = ({ products = []  ,  handleFavourite =()=>{}, addToFavorite= () => {}, addToCart = () => {} }) => {
  return (
    <div className={styles.productsContainer}>

      {products.map(product => (
        <ProductCard
            key={product.id}
            product={product}
            isFavourite={product.isFavourite}
            addToCart={addToCart}
            handleFavourite={handleFavourite}
            addToFavorite={addToFavorite}

            />
      ))}
    </div>
  );
}
/*
const ProductsContainer = ({ products = [], handleFavourite = () => {}, addToFavorite = () => {}, addToCart = () => {} }) => {
    return (
        <div className={styles.productsContainer}>
            {products.map(({ id, name, image, shortDescription, price, isFavourite } ) => (
                <ProductCard
                    key={id}
                    id={id}
                    name={ name}
                    image={image}
                    shortDescription={shortDescription}
                    price={price}
                    isFavourite={isFavourite}
                    addToCart={addToCart}
                    handleFavourite={handleFavourite}
                    addToFavorite={addToFavorite}
                />
            ))}
        </div>
    );
}
*/


ProductsContainer.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string,
      shortDescription: PropTypes.string,
      image: PropTypes.string,
      price: PropTypes.number,
    })
  ),
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func,
    handleFavourite: PropTypes.func,
};

export default ProductsContainer;
