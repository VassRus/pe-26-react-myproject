import { NavLink } from 'react-router-dom';
import styles from './Header.module.scss';
import { useLocation } from 'react-router-dom';
import CartIcon from '../svg/CartIcon';
import Favorite from "../svg/Favorite/index.jsx";
import PropTypes from "prop-types";


const Header = ({cartTotalCount = 0, favoriteTotalCount=0}) => {

    const location = useLocation();

  const isRed = location.pathname === '/login';

  return (
    <header className={styles.header} style={{ background: isRed ? 'red' : null }}>
      <span className={styles.logo}>React Router</span>

      <nav>
        <ul>
          <li>
            <NavLink end to="/" className={({ isActive }) => isActive && styles.active}>Home</NavLink>
          </li>

          <li>
              <NavLink end to="/favorite" className={({ isActive }) => isActive && styles.active}>
                <div>
                    <Favorite/>
                     <span>{favoriteTotalCount}</span>
                </div>
             </NavLink>
          </li>


          <li>
            <NavLink end to="/cart" className={({ isActive }) => isActive && styles.active}>
              <div >
                <CartIcon />
                <span>{cartTotalCount}</span>
              </div>
            </NavLink>
          </li>

          <li>
            <NavLink end to="/login" className={({ isActive }) => isActive && styles.active}>
              <span>Log In</span>
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  )
}
Header.propTypes = {
    cartTotalCount: PropTypes.number,
    favoriteTotalCount: PropTypes.number,
};
export default Header;
