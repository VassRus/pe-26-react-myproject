import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';
import CartDelete from "../svg/CartDelete/index.jsx";


function CartItem({ 
  item = {},
  decrementCartItem = () => {},
  incrementCartItem = () => {},
  deleteCartItem = ()=> {},

 }) {
   /* const {id,name,image,price,count}= item*/

    const totalPrice = item.price * item.count;

  return (
    <div className={styles.item}>
      <img src={item.image} alt={item.name} />
      <h2 className={styles.productName}> {item.name} </h2>

        <button disabled={item.count <= 1} onClick={() => {decrementCartItem(item.id)}}> - </button>
        <p className={styles.count}> Count: {item.count} </p>
        <button onClick={() => {incrementCartItem(item.id)}}> + </button>

      <p className={styles.productPrice}> Price: {item.price} </p>
      <p className={styles.count}> Total: {totalPrice} </p>

       {/* <CartDelete />
        <span>Delete</span>*/}

        <p onClick={() => {deleteCartItem(item.id)}}>
            <CartDelete />
            <span>Delete</span>
        </p>
    </div>
  );
}

CartItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.number,
    count: PropTypes.number,
  }),
  addToCart: PropTypes.func,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  deleteCartItem: PropTypes.func,
};

export default CartItem;
