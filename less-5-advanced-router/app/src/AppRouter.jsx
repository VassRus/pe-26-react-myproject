import { Routes, Route, Navigate } from 'react-router-dom';
import HomeRoute from './routes/HomeRoute';
import CartRoute from './routes/CartRoute';
import LoginRoute from './routes/LoginRoute';
import Favorite from "./routes/Favorite/index.jsx";
import PropTypes from 'prop-types';


const AppRouter = ({
   products = [],
   user = null,
   setUser = () => { },
   addToCart = () => { },
   addToFavorite = ()=>{},
   handleFavourite= ()=>{},
   cart = [],
   decrementCartItem = () => {},
   incrementCartItem = () => {},
    deleteCartItem = () =>{},
}) => {

  return (
    <Routes>
      <Route path="/" element={user ? <HomeRoute addToCart={addToCart} handleFavourite={handleFavourite} addToFavorite={addToFavorite} products={products} /> : <Navigate to='login' />} />
      <Route path="/cart" element={<CartRoute cart={cart} decrementCartItem={decrementCartItem} incrementCartItem={incrementCartItem} deleteCartItem={deleteCartItem}/>} />
      <Route path="/favorite" element={<Favorite  />} />
      <Route path="/login" element={<LoginRoute setUser={setUser} />} />
    </Routes>
  )
}

AppRouter.propTypes = {
  products: PropTypes.array,
  cart: PropTypes.array,
  user: PropTypes.object,
  setUser: PropTypes.func,
  addToCart: PropTypes.func,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
    addToFavorite: PropTypes.func,
    handleFavourite: PropTypes.func,
};

export default AppRouter;