const CATR_LS_KEY = 'cart';
const FAVORITE_LS_KEY = 'favorite';
export const setCartToLS = (cart) => {
    localStorage.setItem(CATR_LS_KEY, JSON.stringify(cart));
}
export const setFavoriteToLS = (favorite) => {
    localStorage.setItem(FAVORITE_LS_KEY, JSON.stringify(favorite));
}

export const getFavoriteFromLS = () => {
    const favorite = localStorage.getItem( FAVORITE_LS_KEY);

    if (favorite) {
        return JSON.parse(favorite);
    } else {
        return [];
    }
}

export const getCartFromLS = () => {
    const cart = localStorage.getItem(CATR_LS_KEY);

    if (cart) {
        return JSON.parse(cart);
    } else {
        return [];
    }
}
