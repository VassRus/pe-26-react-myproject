import './App.scss'
import AppRoutes from "./AppRoutes.jsx";
import Header from "./components/Header/index.jsx";
import Modal from "./components/Modal/index.jsx";

function App() {

  return (
    <>
      <Header />
      <main>
        <h1>Hello</h1>

        <AppRoutes />
        <Modal />
      </main>
    </>
  )
}

export default App
