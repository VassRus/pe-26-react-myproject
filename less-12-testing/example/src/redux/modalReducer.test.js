import { expect } from '@jest/globals';
import modalReducer from './modalSlice';

describe('modalReducer works', () => {
    test('should modalReducer open modal', () => {
        const state = { isOpen: false };
        const action = { type: 'modal/openModal'};

        expect(modalReducer(state, action)).toEqual({ isOpen: true })
    });

    test('should modalReducer close modal', () => {
        const state = { isOpen: true };
        const action = { type: 'modal/closeModal'};

        expect(modalReducer(state, action)).toEqual({ isOpen: false })
    });

    
    test('should modalReducer set title', () => {
        const state = { title: '' };
        const action = { type: 'modal/setTitle', payload: 'new title'};

        expect(modalReducer(state, action)).toEqual({ title: 'new title' })
    });
});
