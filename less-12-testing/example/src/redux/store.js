import { configureStore } from "@reduxjs/toolkit";
import modal from './modalSlice.js';

const store = configureStore({
  reducer: {
    modal,
  }
});

export default store;
