import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  isOpen: false,
  title: ''
}

const modalSlice = createSlice({
  name: 'modal',
  initialState: initialState,
  reducers: {
    openModal: (state) => {
      state.isOpen = true;
    },

    closeModal: (state) => {
      state.isOpen = false;
    },

    setTitle: (state, action) => {
      state.title = action.payload;
    }
  }
});


export default modalSlice.reducer;

export const { openModal, closeModal, setTitle } = modalSlice.actions;
