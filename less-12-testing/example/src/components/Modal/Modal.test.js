import { expect } from '@jest/globals';

import Modal from '.';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider, useDispatch } from 'react-redux';
import store from '../../redux/store';
import { openModal } from '../../redux/modalSlice';

// компонент пишемо для того щоб відкрити модалку якщо Modal то буде пустий дівак
const Wrapper = () => {
    const dispatch = useDispatch();

    return (
        <>
            <button onClick={() => { dispatch(openModal()) }}>open</button>
            <Modal />
        </>
    )
}


describe('Modal open/close', () => {

    test('should render on open', () => {
        render(
            <Provider store={store}>
                <Wrapper />
            </Provider>
        )

        const openBtn = screen.getByText('open');
        fireEvent.click(openBtn);

        expect(screen.getByTestId('modal')).toBeInTheDocument();

        fireEvent.click(screen.getByText('No'));

        expect(screen.queryByTestId('modal')).not.toBeInTheDocument();
    });
});
