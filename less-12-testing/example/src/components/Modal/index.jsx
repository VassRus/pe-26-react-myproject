import styles from './Modal.module.scss';
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../redux/modalSlice.js";
import Button from "../Button/index.jsx";

const Modal = () => {
  const isOpen = useSelector(state => state.modal.isOpen);
  const dispatch = useDispatch();

  if (!isOpen) return null;

  return (
    <div className={styles.root} data-testid="modal">
      <div className={styles.background} />

      <div className={styles.content}>
        <h2>Hello from modal</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, expedita?</p>

        <div className={styles.buttonContainer}>
          <Button onClick={() => { dispatch(closeModal()) }}>No</Button>
          <Button>Yes</Button>
        </div>
      </div>
    </div>
  )
}

export default Modal;
