import classNames from "classnames";
import styles from './Button.module.scss';
import PropTypes from "prop-types";

const Button = ({ children = null, className = '', onClick = () => {} }) => {

  return <button className={classNames(styles.btn, className)} onClick={onClick}>{ children }</button>
}

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onClick: PropTypes.func,
}

export default Button;
