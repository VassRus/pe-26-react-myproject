import { expect } from '@jest/globals';

import Button from ".";
import { render, screen, fireEvent } from '@testing-library/react';

// тест snapshot

describe('Button snapshot testing', () => {
    test('should Button render', () => {

   /*     const resalt = render(<Button className="some-btn">Hello</Button>);
        console.log(resalt)//{ asFragment } властивість render для отримання snapshots */

        const { asFragment } = render(<Button className="some-btn">Hello</Button>);
        console.log(asFragment)
        expect(asFragment()).toMatchSnapshot();
    });
});


//  тест onClick
describe('Button onClick works', () => {
    test('should onClick works', () => {
        const callback = jest.fn();// заглушка що функція рендериться для перевірки через .toBeCalled()
        render(<Button onClick={callback}>Hello</Button>);

        const btn = screen.getByText('Hello'); // находимо кнопку

        fireEvent.click(btn); // клікаємо на кнопку

        // eslint-disable-next-line testing-library/no-debugging-utils
      //  screen.debug();

        expect(callback).toBeCalled(); // був викликаний
    });
});
