import PropTypes from 'prop-types';
import Button from '../Button';
import { useEffect, useState } from 'react';

const Test = ({ isButton = false }) => {
    const [isTitle, setIsTitle] = useState(false);
    const [isParagraph, setIsParagraph] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setIsParagraph(true)
        }, 100);
    }, [])

    return (
        <div>
            <p>Lorem ipsum dolor sit.</p>

            {isButton && <Button onClick={() => { setIsTitle(prev => !prev) }}>Hello from button</Button>}

            {isTitle && <h1>TITLE</h1>}
            {isParagraph && <p>WORLD</p>}
        </div>
    )
}

Test.propTypes = {
    isButton: PropTypes.bool,
}

export default Test;
