import { expect } from '@jest/globals';

import Test from '.';
import { render, screen, fireEvent } from '@testing-library/react';

// тест перевірки snapshot
describe('Button snapshot testing', () => {
    test('should Test render', () => {
        const { asFragment } = render(<Test />);

        expect(asFragment()).toMatchSnapshot();
    });

    test('should Test with button render', () => {
        const { asFragment } = render(<Test isButton={true} />);

        expect(asFragment()).toMatchSnapshot();
    });
});
//  тест перевірки onClick works
describe('Test onClick works', () => {
    test('should Title shows on click', () => {
        render(<Test isButton={true} />);

       // expect(screen.queryByText('TITLE')).not.toBeInTheDocument();

        const btn = screen.getByText('Hello from button');
        fireEvent.click(btn);
        // eslint-disable-next-line testing-library/no-debugging-utils
        //screen.debug()
        expect(screen.getByText('TITLE')).toBeInTheDocument();
    });
});

//  тест перевірки paragrapgh shows after timeot
describe('Test paragrapgh shows after timeot', () => {
    test('should render <p> after some time', async () => {
        render(<Test isButton={true} />);

        const title = await screen.findByText('WORLD');
         // eslint-disable-next-line testing-library/no-debugging-utils
        //screen.debug()
        expect(title).toBeInTheDocument();
    });
});

