import { expect } from '@jest/globals';

import Header from '.';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

describe('Header snapshot testing', () => {
    test('should Header render', () => {
        const { asFragment } = render(<BrowserRouter>
                                             <Header />
                                       </BrowserRouter>
        );

        expect(asFragment()).toMatchSnapshot();
    });
});
