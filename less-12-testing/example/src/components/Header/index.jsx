import { NavLink } from "react-router-dom";
import styles from './Header.module.scss';

const Header = () => (
  <header className={styles.header}>
    <nav>
      <NavLink to={'/'}>Home</NavLink>
      <NavLink to={'/blog'}>Blog</NavLink>
    </nav>
  </header>
)

export default Header;
