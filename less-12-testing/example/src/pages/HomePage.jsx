import Button from "../components/Button/index.jsx";
import { useDispatch } from "react-redux";
import { openModal } from "../redux/modalSlice.js";

const HomePage = () => {
  console.log('Home');
  const dispatch = useDispatch();

  return (
    <>
      <h2>Home</h2>
      <Button onClick={() => {dispatch(openModal())}}>Open Modal</Button>
    </>
  )
}

export default HomePage;
