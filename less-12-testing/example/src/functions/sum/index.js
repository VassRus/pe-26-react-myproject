/**
 * @param a {number}
 * @param b {number}
 * @returns {number}
 */
const sum = (a, b) => {
    if (typeof a !== 'number' || typeof b !== 'number') {
        return 0;
    }

    return a + b;
}

export default sum;