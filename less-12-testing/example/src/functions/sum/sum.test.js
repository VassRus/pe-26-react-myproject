
import sum from "./index.js";

describe('Sum function works', () => {

    test('should return valid sum', () => {
        expect(sum(2, 5)).toBe(7)
    });

    test('should return valid sum ith another values', () => {
        expect(sum(6, 90)).toBe(96);
    });
});


describe('Sum handle bad cases', () => {

    test('should return 0 if no numbers in arguments', () => {
        expect(sum('2', null)).toBe(0)
    });


});
