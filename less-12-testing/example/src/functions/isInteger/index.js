/**
 * @param num {number}
 * @returns {boolean}
 */ 
const isInteger = (num) => {
    return Number.isInteger(num);
}

export default isInteger;