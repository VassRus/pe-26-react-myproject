import isInteger from ".";

describe('isInteger function works', () => {

  test('should return true if integer', () => {
      expect(isInteger(2)).toBeTruthy()
  });

  test('should return false if float', () => {
      expect(isInteger(2.5)).toBeFalsy();
  });
});

describe('isInteger bad cases', () => {

  test('should return false with bad values', () => {
    expect(isInteger(null)).toBeFalsy();
    expect(isInteger()).toBeFalsy();
    expect(isInteger('2')).toBeFalsy();
  })
})