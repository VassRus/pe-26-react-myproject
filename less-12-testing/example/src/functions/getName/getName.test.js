import { expect } from '@jest/globals';

import getName from ".";

const callback = jest.fn(); // заглушка функція

describe('getName run additional callback', () => {

    test('should additionalCallback called', () => {
        getName(null, callback);
        getName(null, callback);
        getName(null, callback);

        expect(callback).toBeCalledTimes(3)
    })
  })