/**
 * @param nameChanger {function}
 * @param additionalCallback {function}
 * @returns {string|*}
 */
const getName = (nameChanger = null, additionalCallback = null) => {
    if (additionalCallback) {
        additionalCallback();
    }

    if (nameChanger) {
        return nameChanger('John');
    }
    return 'John'
}

export default getName;