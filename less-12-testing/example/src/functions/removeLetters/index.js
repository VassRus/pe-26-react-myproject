/**
 * @param str {string}
 * @returns {string}
 */
const removeLetter = (str) => {
    return str.replace(/[A-Za-z]/gi, '');
}

export default removeLetter;